// ----------------------------------------------------------------------------
// <copyright file="Player.cs" company="Exit Games GmbH">
//   Loadbalancing Framework for Photon - Copyright (C) 2011 Exit Games GmbH
// </copyright>
// <summary>
//   Per client in a room, a Player is created. This client's Player is also
//   known as PhotonClient.LocalPlayer and the only one you might change 
//   properties for.
// </summary>
// <author>developer@exitgames.com</author>
// ----------------------------------------------------------------------------

namespace ExitGames.Client.Photon.LoadBalancing
{
    using System;
    using System.Collections;
    using ExitGames.Client.Photon;

    /// <summary>
    /// Summarizes a "player" within a room, identified (in that room) by actorID.
    /// </summary>
    /// <remarks>
    /// Each player has a actorID, valid for that room. It's -1 until assigned by server.
    /// </remarks>
    public class Player
    {
		#region Fields only for Unity BootCamp
        public PlayerRemote playerRemote;
		public UnityEngine.Transform playerTransform;
		internal const byte EV_PLAYER_INFO = 100;
		#endregion
		
        /// <summary>Backing field for property.</summary>
        private int actorID = -1;


        /// <summary>Only one player is controlled by each client. Others are not local.</summary>
        public readonly bool IsLocal;

        /// <summary>
        /// A reference to the LoadbalancingClient which is currently keeping the connection and state.
        /// </summary>
        protected internal LoadBalancingClient LoadBalancingClient { get; set; }

        /// <summary>
        /// Used internally to identify the masterclient of a room.
        /// </summary>
        protected internal Room RoomReference { get; set; }

        /// <summary>Identifier of this player in current room. Also known as: actorNumber or actorID. It's -1 outside of rooms.</summary>
        /// <remarks>The ID is assigned per room and only valid in that context. It will change even on leave and re-join.</remarks>
        public int ID
        {
            get { return this.actorID; }
        }

        /// <summary>Background field for Name.</summary>
        private string name;

        /// <summary>Nickname of this player. Also in Properties.</summary>
        /// <remarks>
        /// A player might change his own playername in a room (it's only a property).
        /// Setting this value updates the server and other players (using an operation).
        /// </remarks>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (!string.IsNullOrEmpty(this.name) && this.name.Equals(value))
                {
                    return;
                }

                this.name = value;

                // update a room, if we changed our name (locally, while being in a room)
                if (this.IsLocal && this.LoadBalancingClient != null && this.LoadBalancingClient.State == ClientState.Joined)
                {
                    this.SetPlayerNameProperty();
                }
            }
        }

        /// <summary>
        /// The player with the lowest actorID is the master and could be used for special tasks.
        /// The LoadBalancingClient.LocalPlayer is not master unless in a room (this is the only player which exists outside of rooms, to store a name).
        /// </summary>
        public bool IsMasterClient
        {
            get
            {
                if (this.RoomReference == null)
                {
                    return false;
                }

                return this.ID == this.RoomReference.MasterClientId;
            }
        }

        /// <summary>Cache for custom properties of player.</summary>
        public Hashtable CustomProperties { get; private set; }

        /// <summary>Creates a Hashtable with all properties (custom and "well known" ones).</summary>
        /// <remarks>If used more often, this should be cached.</remarks>
        public Hashtable AllProperties
        {
            get
            {
                Hashtable allProps = new Hashtable();
                allProps.Merge(this.CustomProperties);
                allProps[ActorProperties.PlayerName] = this.name;
                return allProps;
            }
        }

        /// <summary>
        /// Creates a player instance.
        /// To extend and replace this Player, override LoadBalancingPeer.CreatePlayer().
        /// </summary>
        /// <param name="name">Name of the player (a "well known property").</param>
        /// <param name="actorID">ID or ActorNumber of this player in the current room (a shortcut to identify each player in room)</param>
        /// <param name="isLocal">If this is the local peer's player (or a remote one).</param>
        protected internal Player(string name, int actorID, bool isLocal)
        {
            this.CustomProperties = new Hashtable();
            this.IsLocal = isLocal;
            this.actorID = actorID;
            this.Name = name;
        }

        /// <summary>
        /// Caches custom properties for this player.
        /// </summary>
        public void CacheProperties(Hashtable properties)
        {
            if (properties == null || properties.Count == 0 || this.CustomProperties.Equals(properties))
            {
                return;
            }

            if (properties.ContainsKey(ActorProperties.PlayerName))
            {
                string nameInServersProperties = (string)properties[ActorProperties.PlayerName];
                if (this.IsLocal)
                {
                    // the local playername is different than in the properties coming from the server
                    // so the local name was changed and the server is outdated -> update server
                    // update property instead of using the outdated name coming from server
                    if (!nameInServersProperties.Equals(this.name))
                    {
                        this.SetPlayerNameProperty();
                    }
                }
                else
                {
                    this.Name = nameInServersProperties;
                }
            }

            this.CustomProperties.MergeStringKeys(properties);
        }

        /// <summary>
        /// Returns name and custom properties.
        /// </summary>
        public override string ToString()
        {
            return this.Name + " " + SupportClass.DictionaryToString(this.CustomProperties);
        }

        /// <summary>
        /// Makes Player comparable
        /// </summary>
        public override bool Equals(object p)
        {
            Player pp = p as Player;
            return (pp != null && this.GetHashCode() == pp.GetHashCode());
        }

        /// <summary>
        /// Accompanies Equals, using the ID (actorNumber) as HashCode to return.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.ID;
        }

        /// <summary>
        /// Used internally, to update this client's playerID when assigned.
        /// </summary>
        protected internal void ChangeLocalID(int newID)
        {
            if (!this.IsLocal)
            {
                //Debug.LogError("ERROR You should never change Player IDs!");
                return;
            }

            this.actorID = newID;
        }

        /// <summary>
        /// Updates the custom properties of this Room with propertiesToSet.
        /// Only string-typed keys are applied, new properties (string keys) are added, existing are updated
        /// and if a value is set to null, this will remove the custom property.
        /// </summary>
        /// <remarks>
        /// Local cache is updated immediately, other players are updated through Photon with a fitting operation.
        /// </remarks>
        /// <param name="propertiesToSet"></param>
        public void SetCustomProperties(Hashtable propertiesToSet)
        {
            Hashtable customProps = propertiesToSet.StripToStringKeys() as Hashtable;

            // merge (delete null-values)
            this.CustomProperties.Merge(customProps);
            this.CustomProperties.StripKeysWithNullValues();

            // send (sync) these new values
            this.LoadBalancingClient.OpSetCustomPropertiesOfActor(this.actorID, customProps);
        }

        /// <summary>Uses OpSetPropertiesOfActor to set this player's name.</summary>
        private void SetPlayerNameProperty()
        {
            Hashtable properties = new Hashtable();
            properties[ActorProperties.PlayerName] = this.name;
			
			//if (this.LoadBalancingClient != null)
			this.LoadBalancingClient.OpSetPropertiesOfActor(this.ID, properties);
        }
    }
}