// ----------------------------------------------------------------------------
// <copyright file="LoadBalancingPeer.cs" company="Exit Games GmbH">
//   Loadbalancing Framework for Photon - Copyright (C) 2011 Exit Games GmbH
// </copyright>
// <summary>
//   Provides the operations needed to use the loadbalancing server app(s).
// </summary>
// <author>developer@exitgames.com</author>
// ----------------------------------------------------------------------------

namespace ExitGames.Client.Photon.LoadBalancing
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using ExitGames.Client.Photon;
    using ExitGames.Client.Photon.Lite;

    /// <summary>
    /// A LoadbalancingPeer provides the operations and enum definitions needed to use the loadbalancing server app(s).
    /// </summary>
    /// <remarks>
    /// The LoadBalancingPeer does not keep a state, instead this is done by a LoadBalancingClient.
    /// </remarks>
    public class LoadBalancingPeer : PhotonPeer
    {
        public LoadBalancingPeer(ConnectionProtocol protocolType) : base(protocolType)
        {
            // this does not require a Listener, so:
            // make sure to set this.Listener before using a peer!
        }

        /// <summary>
        /// Joins the lobby on the Master Server, where you get a list of RoomInfos of currently open rooms.
        /// This is an async request which triggers a OnOperationResponse() call.
        /// </summary>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpJoinLobby()
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpJoinLobby()");
            }

            return this.OpCustom(OperationCode.JoinLobby, null, true);
        }

        /// <summary>
        /// Leaves the lobby on the Master Server.
        /// This is an async request which triggers a OnOperationResponse() call.
        /// </summary>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpLeaveLobby()
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpLeaveLobby()");
            }

            return this.OpCustom(OperationCode.LeaveLobby, null, true);
        }

        /// <summary>
        /// Creates a room (on either Master or Game Server).
        /// The response depends on the server the peer is connected to: 
        /// Master will return a Game Server to connect to.
        /// Game Server will return the Room's data.
        /// This is an async request which triggers a OnOperationResponse() call.
        /// </summary>
        /// <param name="roomName"></param>
        /// <param name="isVisible"></param>
        /// <param name="isOpen"></param>
        /// <param name="maxPlayers"></param>
        /// <param name="customGameProperties"></param>
        /// <param name="playerProperties"></param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpCreateRoom(string roomName, bool isVisible, bool isOpen, byte maxPlayers, Hashtable customGameProperties, Hashtable playerProperties)
        {
            return OpCreateRoom(roomName, isVisible, isOpen, maxPlayers, customGameProperties, playerProperties, null);
        }

        /// <summary>
        /// Creates a room (on either Master or Game Server).
        /// The response depends on the server the peer is connected to: 
        /// Master will return a Game Server to connect to.
        /// Game Server will return the Room's data.
        /// This is an async request which triggers a OnOperationResponse() call.
        /// </summary>
        /// <param name="roomName">The name of this room. Must be unique. Pass null to get a name assigned server-side.</param>
        /// <param name="isVisible">Defines if this room is listed in the lobby. If not, it also is not joined randomly.</param>
        /// <param name="isOpen">Defines if this room can be joined at all.</param>
        /// <param name="maxPlayers">Max number of players that can be in the room at any time. 0 means "no limit".</param>
        /// <param name="customGameProperties">The room's custom properties to set. Use string keys!</param>
        /// <param name="playerProperties">This player's custom properties. Use string keys!</param>
        /// <param name="propsListedInLobby">Defines the custom room properties that get listed in the lobby. Null defaults to "none", a string[0].</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpCreateRoom(string roomName, bool isVisible, bool isOpen, byte maxPlayers, Hashtable customGameProperties, Hashtable playerProperties, string[] propsListedInLobby)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpCreateRoom()");
            }

            Hashtable gameProperties = new Hashtable();
            gameProperties[GameProperties.IsOpen] = isOpen;
            gameProperties[GameProperties.IsVisible] = isVisible;
            gameProperties[GameProperties.PropsListedInLobby] = (propsListedInLobby == null) ? new string[0] : propsListedInLobby;
            gameProperties.MergeStringKeys(customGameProperties);
            if (maxPlayers > 0)
            {
                gameProperties[GameProperties.MaxPlayers] = maxPlayers;
            }

            Dictionary<byte, object> op = new Dictionary<byte, object>();
            op[ParameterCode.GameProperties] = gameProperties;
            op[ParameterCode.CleanupCacheOnLeave] = true;   // Note: This is not yet optional. It makes sense to get this into the API when caching is explained, too
            op[ParameterCode.Broadcast] = true;
            if (playerProperties != null)
            {
                op[ParameterCode.PlayerProperties] = playerProperties;
            }

            if (!string.IsNullOrEmpty(roomName))
            {
                op[ParameterCode.RoomName] = roomName;
            }

            return this.OpCustom(OperationCode.CreateGame, op, true);
        }

        /// <summary>
        /// Joins a room by name and sets this player's properties.
        /// </summary>
        /// <param name="roomName"></param>
        /// <param name="playerProperties"></param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpJoinRoom(string roomName, Hashtable playerProperties)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpJoinRoom()");
            }

            if (string.IsNullOrEmpty(roomName))
            {
                this.Listener.DebugReturn(DebugLevel.ERROR, "OpJoinRoom() failed. Please specify a roomname.");
                return false;
            }

            Dictionary<byte, object> op = new Dictionary<byte, object>();
            op[ParameterCode.RoomName] = roomName;
            op[ParameterCode.Broadcast] = true;
            if (playerProperties != null)
            {
                op[ParameterCode.PlayerProperties] = playerProperties;
            }

            //Listener.DebugReturn(DebugLevel.INFO, OperationCode.JoinGame + ": " + SupportClass.DictionaryToString(op));
            return this.OpCustom(OperationCode.JoinGame, op, true);
        }

        /// <summary>
        /// Operation to join a random, available room. 
        /// This operation fails if all rooms are closed or full.
        /// If successful, the result contains a gameserver address and the name of some room.
        /// </summary>
        /// <param name="expectedCustomRoomProperties">Optional. A room will only be joined, if it matches these custom properties (with string keys).</param>
        /// <param name="expectedMaxPlayers">Filters for a particular maxplayer setting. Use 0 to accept any maxPlayer value.</param>
        /// <returns>If the operation could be sent currently (requires connection).</returns>
        public virtual bool OpJoinRandomRoom(Hashtable expectedCustomRoomProperties, byte expectedMaxPlayers)
        {
            return this.OpJoinRandomRoom(expectedCustomRoomProperties, expectedMaxPlayers, null);
        }

        /// <summary>
        /// Operation to join a random, available room. Overload taking additional player  propertiers. 
        /// This operation fails if all rooms are closed or full.
        /// If successful, the result contains a gameserver address and the name of some room.
        /// </summary>
        /// <param name="expectedCustomRoomProperties">Optional. A room will only be joined, if it matches these custom properties (with string keys).</param>
        /// <param name="expectedMaxPlayers">Filters for a particular maxplayer setting. Use 0 to accept any maxPlayer value.</param>
        /// <returns>If the operation could be sent currently (requires connection).</returns>
        public virtual bool OpJoinRandomRoom(Hashtable expectedCustomRoomProperties, byte expectedMaxPlayers, Hashtable playerProperties)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpJoinRandomRoom()");
            }

            Hashtable expectedRoomProperties = new Hashtable();
            expectedRoomProperties.MergeStringKeys(expectedCustomRoomProperties);
            if (expectedMaxPlayers > 0)
            {
                expectedRoomProperties[GameProperties.MaxPlayers] = expectedMaxPlayers;
            }

            Dictionary<byte, object> opParameters = new Dictionary<byte, object>();
            if (expectedRoomProperties != null && expectedRoomProperties.Count > 0)
            {
                opParameters[ParameterCode.Properties] = expectedRoomProperties;
            }

            if (playerProperties != null && playerProperties.Count > 0)
            {
                opParameters[ParameterCode.PlayerProperties] = playerProperties;
            }

            Listener.DebugReturn(DebugLevel.INFO, OperationCode.JoinRandomGame + ": " + SupportClass.DictionaryToString(opParameters));
            return this.OpCustom(OperationCode.JoinRandomGame, opParameters, true);
        }

        /// <summary>
        /// Sets custom properties of a player / actor (only passing on the string-typed custom properties).
        /// Internally this uses OpSetProperties, which can be used to either set room or player properties.
        /// </summary>
        /// <param name="actorNr">The payer ID (a.k.a. actorNumber) of the player to attach these properties to.</param>
        /// <param name="actorProperties">The custom properties to add or update.</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public bool OpSetCustomPropertiesOfActor(int actorNr, Hashtable actorProperties)
        {
            return this.OpSetPropertiesOfActor(actorNr, actorProperties.StripToStringKeys());
        }

        /// <summary>
        /// Sets properties of a player / actor.
        /// Internally this uses OpSetProperties, which can be used to either set room or player properties.
        /// </summary>
        /// <param name="actorNr">The payer ID (a.k.a. actorNumber) of the player to attach these properties to.</param>
        /// <param name="actorProperties">The properties to add or update.</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        protected internal virtual bool OpSetPropertiesOfActor(int actorNr, Hashtable actorProperties)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpSetPropertiesOfActor()");
            }

            Dictionary<byte, object> opParameters = new Dictionary<byte, object>();
            opParameters.Add(ParameterCode.Properties, actorProperties);
            opParameters.Add(ParameterCode.ActorNr, actorNr);
            opParameters.Add(ParameterCode.Broadcast, true);
            
            return this.OpCustom((byte)OperationCode.SetProperties, opParameters, true, 0, false);
        }

        /// <summary>
        /// Set a "well known" property of a room.
        /// Internally this uses OpSetProperties, which can be used to either set room or player properties.
        /// </summary>
        /// <param name="propCode"></param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        protected internal void OpSetPropertyOfRoom(byte propCode, object value)
        {
            Hashtable properties = new Hashtable();
            properties[propCode] = value;
            this.OpSetPropertiesOfRoom(properties);
        }

        /// <summary>
        /// Sets custom properties of a room (only passing string-typed keys in the Hashtable).
        /// Internally this uses OpSetProperties, which can be used to either set room or player properties.
        /// </summary>
        /// <param name="gameProperties"></param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public bool OpSetCustomPropertiesOfRoom(Hashtable gameProperties)
        {
            return this.OpSetPropertiesOfRoom(gameProperties.StripToStringKeys());
        }

        /// <summary>
        /// Sets properties of a room.
        /// Internally this uses OpSetProperties, which can be used to either set room or player properties.
        /// </summary>
        /// <param name="gameProperties"></param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        protected internal virtual bool OpSetPropertiesOfRoom(Hashtable gameProperties)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpSetPropertiesOfRoom()");
            }

            Dictionary<byte, object> opParameters = new Dictionary<byte, object>();
            opParameters.Add(ParameterCode.Properties, gameProperties);
            opParameters.Add(ParameterCode.Broadcast, true);

            return this.OpCustom((byte)OperationCode.SetProperties, opParameters, true, 0, false);
        }

        /// <summary>
        /// Sends this app's appId and appVersion to identify this application server side.
        /// </summary>
        /// <remarks>
        /// This operation makes use of encryption, if it's established beforehand.
        /// See: EstablishEncryption(). Check encryption with IsEncryptionAvailable.
        /// </remarks>
        /// <param name="appId"></param>
        /// <param name="appVersion"></param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpAuthenticate(string appId, string appVersion)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpAuthenticate()");
            }

            Dictionary<byte, object> opParameters = new Dictionary<byte, object>();
            opParameters[ParameterCode.AppVersion] = appVersion;
            opParameters[ParameterCode.ApplicationId] = appId;

            return this.OpCustom(OperationCode.Authenticate, opParameters, true, (byte)0, this.IsEncryptionAvailable);
        }

        /// <summary>
        /// Used in a room to raise (send) an event to the other players. 
        /// Multiple overloads expose different parameters to this frequently used operation.
        /// </summary>
        /// <param name="eventCode">Code for this "type" of event (use a code per "meaning" or content).</param>
        /// <param name="evData">Data to send. Hashtable that contains key-values of Photon serializable datatypes.</param>
        /// <param name="sendReliable">Use false if the event is replaced by a newer rapidly. Reliable events add overhead and add lag when repeated.</param>
        /// <param name="channelId">The "channel" to which this event should belong. Per channel, the sequence is kept in order.</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpRaiseEvent(byte eventCode, Hashtable evData, bool sendReliable, byte channelId)
        {
            return this.OpRaiseEvent(eventCode, evData, sendReliable, channelId, EventCaching.DoNotCache, ReceiverGroup.Others);
        }

        /// <summary>
        /// Used in a room to raise (send) an event to the other players. 
        /// Multiple overloads expose different parameters to this frequently used operation.
        /// </summary>
        /// <param name="eventCode">Code for this "type" of event (use a code per "meaning" or content).</param>
        /// <param name="evData">Data to send. Hashtable that contains key-values of Photon serializable datatypes.</param>
        /// <param name="sendReliable">Use false if the event is replaced by a newer rapidly. Reliable events add overhead and add lag when repeated.</param>
        /// <param name="channelId">The "channel" to which this event should belong. Per channel, the sequence is kept in order.</param>
        /// <param name="targetActors">Defines the target players who should receive the event (use only for small target groups).</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpRaiseEvent(byte eventCode, Hashtable evData, bool sendReliable, byte channelId, int[] targetActors)
        {
            return this.OpRaiseEvent(eventCode, evData, sendReliable, channelId, targetActors, EventCaching.DoNotCache);
        }

        /// <summary>
        /// Used in a room to raise (send) an event to the other players. 
        /// Multiple overloads expose different parameters to this frequently used operation.
        /// </summary>
        /// <param name="eventCode">Code for this "type" of event (use a code per "meaning" or content).</param>
        /// <param name="evData">Data to send. Hashtable that contains key-values of Photon serializable datatypes.</param>
        /// <param name="sendReliable">Use false if the event is replaced by a newer rapidly. Reliable events add overhead and add lag when repeated.</param>
        /// <param name="channelId">The "channel" to which this event should belong. Per channel, the sequence is kept in order.</param>
        /// <param name="targetActors">Defines the target players who should receive the event (use only for small target groups).</param>
        /// <param name="cache">Use EventCaching options to store this event for players who join.</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpRaiseEvent(byte eventCode, Hashtable evData, bool sendReliable, byte channelId, int[] targetActors, EventCaching cache)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpRaiseEvent()");
            }

            Dictionary<byte, object> opParameters = new Dictionary<byte, object>();
            opParameters[ParameterCode.Data] = evData;
            opParameters[ParameterCode.Code] = (byte)eventCode;

            if (cache != EventCaching.DoNotCache)
            {
                opParameters[ParameterCode.Cache] = (byte)cache;
            }

            if (targetActors != null)
            {
                opParameters[ParameterCode.ActorList] = targetActors;
            }

            return this.OpCustom(OperationCode.RaiseEvent, opParameters, sendReliable, channelId);
        }

        /// <summary>
        /// Used in a room to raise (send) an event to the other players. 
        /// Multiple overloads expose different parameters to this frequently used operation.
        /// </summary>
        /// <param name="eventCode">Code for this "type" of event (use a code per "meaning" or content).</param>
        /// <param name="evData">Data to send. Hashtable that contains key-values of Photon serializable datatypes.</param>
        /// <param name="sendReliable">Use false if the event is replaced by a newer rapidly. Reliable events add overhead and add lag when repeated.</param>
        /// <param name="channelId">The "channel" to which this event should belong. Per channel, the sequence is kept in order.</param>
        /// <param name="cache">Use EventCaching options to store this event for players who join.</param>
        /// <param name="receivers">ReceiverGroup defines to which group of players the event is passed on.</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public virtual bool OpRaiseEvent(byte eventCode, Hashtable evData, bool sendReliable, byte channelId, EventCaching cache, ReceiverGroup receivers)
        {
            if (this.DebugOut == DebugLevel.INFO)
            {
                this.Listener.DebugReturn(DebugLevel.INFO, "OpRaiseEvent()");
            }

            Dictionary<byte, object> opParameters = new Dictionary<byte, object>();
            opParameters[ParameterCode.Data] = evData;
            opParameters[ParameterCode.Code] = (byte)eventCode;

            if (receivers != ReceiverGroup.Others)
            {
                opParameters[ParameterCode.ReceiverGroup] = (byte)receivers;
            }

            if (cache != EventCaching.DoNotCache)
            {
                opParameters[ParameterCode.Cache] = (byte)cache;
            }

            return this.OpCustom((byte)OperationCode.RaiseEvent, opParameters, sendReliable, channelId);
        }
    }

    public class ErrorCode
    {
        /// <summary>(0) is always "OK", anything else an error or specific situation.</summary>
        public const int Ok = 0;

        // server - Photon low(er) level: <= 0
        public const int OperationNotAllowedInCurrentState = -3;
        /// <summary></summary>
        public const int InvalidOperationCode = -2;
        /// <summary></summary>
        public const int InternalServerError = -1;

        // server - PhotonNetwork: 0x7FFF and down
        // logic-level error codes start with short.max

        /// <summary>Authentication failed. Possible cause: AppId is unknown to Photon (in cloud service).</summary>
        public const int InvalidAuthentication = 0x7FFF;
        /// <summary></summary>
        public const int GameIdAlreadyExists = 0x7FFF - 1;
        /// <summary></summary>
        public const int GameFull = 0x7FFF - 2;
        /// <summary></summary>
        public const int GameClosed = 0x7FFF - 3;
        /// <summary></summary>
        public const int AlreadyMatched = 0x7FFF - 4;
        /// <summary></summary>
        public const int ServerFull = 0x7FFF - 5;
        /// <summary></summary>
        public const int UserBlocked = 0x7FFF - 6;
        /// <summary></summary>
        public const int NoRandomMatchFound = 0x7FFF - 7;
        /// <summary></summary>
        public const int GameDoesNotExist = 0x7FFF - 9;
    }


    /// <summary>
    /// These (byte) values define "well known" properties for an Actor / Player.
    /// </summary>
    /// <remarks>
    /// "Custom properties" have to use a string-type as key. They can be assigned at will.
    /// </remarks>
    public class ActorProperties
    {
        /// <summary>(255) Name of a player/actor.</summary>
        public const byte PlayerName = 255; // was: 1
    }

    /// <summary>
    /// These (byte) values are for "well known" room/game properties used in Photon Loadbalancing.
    /// </summary>
    /// <remarks>
    /// "Custom properties" have to use a string-type as key. They can be assigned at will.
    /// </remarks>
    public class GameProperties
    {   
        /// <summary>(255) Max number of players that "fit" into this room. 0 is for "unlimited".</summary>
        public const byte MaxPlayers = 255;
        /// <summary>(254) Makes this room listed or not in the lobby on master.</summary>
        public const byte IsVisible = 254;
        /// <summary>(253) Allows more players to join a room (or not).</summary>
        public const byte IsOpen = 253;
        /// <summary>(252) Current count od players in the room. Used only in the lobby on master.</summary>
        public const byte PlayerCount = 252;
        /// <summary>(251) True if the room is to be removed from room listing (used in update to room list in lobby on master)</summary>
        public const byte Removed = 251;
        /// <summary>(250) A list of the room properties to pass to the RoomInfo list in a lobby. This is used in CreateRoom, which defines this list once per room.</summary>
        public const byte PropsListedInLobby = 250;
    }

    /// <summary>
    /// These values are for events defined by Photon Loadbalancing.
    /// </summary>
    /// <remarks>They start at 255 and go DOWN. Your own in-game events can start at 0.</remarks>
    public class EventCode
    {
        /// <summary>(230) Initial list of RoomInfos (in lobby on Master)</summary>
        public const byte GameList = 230;
        /// <summary>(229) Update of RoomInfos to be merged into "initial" list (in lobby on Master)</summary>
        public const byte GameListUpdate = 229;
        /// <summary>(228) Currently not used. State of queueing in case of server-full</summary>
        public const byte QueueState = 228;
        /// <summary>(227) Currently not used. Event for matchmaking</summary>
        public const byte Match = 227;
        /// <summary>(226) Event with stats about this application (players, rooms, etc)</summary>
        public const byte AppStats = 226;
        /// <summary>(210) Internally used in case of hosting by Azure</summary>
        public const byte AzureNodeInfo = 210;
        /// <summary>(255) Event Join: someone joined the game. The new actorNumber is provided as well as the properties of that actor (if set in OpJoin).</summary>
        public const byte Join = (byte)LiteEventCode.Join;
        /// <summary>(254) Event Leave: The player who left the game can be identified by the actorNumber.</summary>
        public const byte Leave = (byte)LiteEventCode.Leave;
        /// <summary>(253) When you call OpSetProperties with the broadcast option "on", this event is fired. It contains the properties being set.</summary>
        public const byte PropertiesChanged = (byte)LiteEventCode.PropertiesChanged;
        /// <summary>(253) When you call OpSetProperties with the broadcast option "on", this event is fired. It contains the properties being set.</summary>
        [Obsolete("Use PropertiesChanged now.")]
        public const byte SetProperties = (byte)LiteEventCode.PropertiesChanged;
    }

    /// <summary>Codes for parameters of Operations and Events.</summary>
    public class ParameterCode
    {
        /// <summary>(230) Address of a (game) server to use.</summary>
        public const byte Address = 230;
        /// <summary>(229) Count of players in this application in a rooms (used in stats event)</summary>
        public const byte PeerCount = 229;
        /// <summary>(228) Count of games in this application (used in stats event)</summary>
        public const byte GameCount = 228;
        /// <summary>(227) Count of players on the master server (in this app, looking for rooms)</summary>
        public const byte MasterPeerCount = 227;
        /// <summary>(225) User's ID</summary>
        public const byte UserId = 225;
        /// <summary>(224) Your application's ID: a name on your own Photon or a GUID on the Photon Cloud</summary>
        public const byte ApplicationId = 224;
        /// <summary>(223) Not used currently. If you get queued before connect, this is your position</summary>
        public const byte Position = 223;
        /// <summary>(222) List of RoomInfos about open / listed rooms</summary>
        public const byte GameList = 222;
        /// <summary>(221) Internally used to establish encryption</summary>
        public const byte Secret = 221;
        /// <summary>(220) Version of your application</summary>
        public const byte AppVersion = 220;
        /// <summary>(210) Internally used in case of hosting by Azure</summary>
        public const byte AzureNodeInfo = 210;	// only used within events, so use: EventCode.AzureNodeInfo
        /// <summary>(209) Internally used in case of hosting by Azure</summary>
        public const byte AzureLocalNodeId = 209;
        /// <summary>(208) Internally used in case of hosting by Azure</summary>
        public const byte AzureMasterNodeId = 208;

        /// <summary>(255) Code fro the gameId/roomName (a unique name per room). Used in OpJoin and similar.</summary>
        public const byte RoomName = (byte)LiteOpKey.GameId;
        /// <summary>(250) Code for broadcast parameter of OpSetProperties method.</summary>
        public const byte Broadcast = (byte)LiteOpKey.Broadcast;
        /// <summary>(252) Code for list of players in a room. Currently not used.</summary>
        public const byte ActorList = (byte)LiteOpKey.ActorList;
        /// <summary>(254) Code of the Actor of an operation. Used for property get and set.</summary>
        public const byte ActorNr = (byte)LiteOpKey.ActorNr;
        /// <summary>(249) Code for property set (Hashtable).</summary>
        public const byte PlayerProperties = (byte)LiteOpKey.ActorProperties;
        /// <summary>(245) Code of data/custom content of an event. Used in OpRaiseEvent.</summary>
        public const byte CustomEventContent = (byte)LiteOpKey.Data;
        /// <summary>(245) Code of data of an event. Used in OpRaiseEvent.</summary>
        public const byte Data = (byte)LiteOpKey.Data;
        /// <summary>(244) Code used when sending some code-related parameter, like OpRaiseEvent's event-code.</summary>
        /// <remarks>This is not the same as the Operation's code, which is no longer sent as part of the parameter Dictionary in Photon 3.</remarks>
        public const byte Code = (byte)LiteOpKey.Code;
        /// <summary>(248) Code for property set (Hashtable).</summary>
        public const byte GameProperties = (byte)LiteOpKey.GameProperties;
        /// <summary>
        /// (251) Code for property-set (Hashtable). This key is used when sending only one set of properties.
        /// If either ActorProperties or GameProperties are used (or both), check those keys.
        /// </summary>
        public const byte Properties = (byte)LiteOpKey.Properties;
        /// <summary>(253) Code of the target Actor of an operation. Used for property set. Is 0 for game</summary>
        public const byte TargetActorNr = (byte)LiteOpKey.TargetActorNr;
        /// <summary>(246) Code to select the receivers of events (used in Lite, Operation RaiseEvent).</summary>
        public const byte ReceiverGroup = (byte)LiteOpKey.ReceiverGroup;
        /// <summary>(247) Code for caching events while raising them.</summary>
        public const byte Cache = (byte)LiteOpKey.Cache;
        /// <summary>(241) Bool parameter of CreateGame Operation. If true, server cleans up roomcache of leaving players (their cached events get removed).</summary>
        public const byte CleanupCacheOnLeave = (byte)241;
    }

    public class OperationCode
    {
        /// <summary>(230) Authenticates this peer and connects to a virtual application</summary>
        public const byte Authenticate = 230;
        /// <summary>(229) Joins lobby (on master)</summary>
        public const byte JoinLobby = 229;
        /// <summary>(228) Leaves lobby (on master)</summary>
        public const byte LeaveLobby = 228;
        /// <summary>(227) Creates a game (or fails if name exists)</summary>
        public const byte CreateGame = 227;
        /// <summary>(226) Join game (by name)</summary>
        public const byte JoinGame = 226;
        /// <summary>(225) Joins random game (on master)</summary>
        public const byte JoinRandomGame = 225;
        
        // public const byte CancelJoinRandom = 224; // obsolete, cause JoinRandom no longer is a "process". now provides result immediately
        
        public const byte Leave = (byte)LiteOpCode.Leave;
        /// <summary>(253) Raise event (in a room, for other actors/players)</summary>
        public const byte RaiseEvent = (byte)LiteOpCode.RaiseEvent;
        /// <summary>(252) Set Properties (of room or actor/player)</summary>
        public const byte SetProperties = (byte)LiteOpCode.SetProperties;
        /// <summary>(251) Get Properties</summary>
        public const byte GetProperties = (byte)LiteOpCode.GetProperties;
    }
}