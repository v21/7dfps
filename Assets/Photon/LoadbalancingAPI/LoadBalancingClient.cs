﻿// -----------------------------------------------------------------------
// <copyright file="LoadBalancingClient.cs" company="Exit Games GmbH">
//   Loadbalancing Framework for Photon - Copyright (C) 2011 Exit Games GmbH
// </copyright>
// <summary>
//   Provides the operations and a state for games using the 
//   Photon LoadBalancing server.
// </summary>
// <author>developer@exitgames.com</author>
// ----------------------------------------------------------------------------

namespace ExitGames.Client.Photon.LoadBalancing
{
    using System;
    using System.Collections;
    using System.Collections.Generic;


    #region Enums

    /// <summary>Possible states for a LoadBalancingClient.</summary>
    public enum ClientState
    {
        /// <summary>Peer is created but not used yet.</summary>
        Uninitialized,
        /// <summary>Not used currently.</summary>
        PeerCreated,
        /// <summary>Connecting to master (includes connect, authenticate and joining the lobby)</summary>
        ConnectingToMasterserver,
        /// <summary>Connected to master server.</summary>
        ConnectedToMaster,
        /// <summary>Currently not used.</summary>
        Queued,
        /// <summary>Usually when Authenticated, the client will join a game or the lobby (if AutoJoinLobby is true).</summary>
        Authenticated,
        /// <summary>Connected to master and joined lobby. Display room list and join/create rooms at will.</summary>
        JoinedLobby,
        /// <summary>Transition from master to game server.</summary>
        DisconnectingFromMasterserver,
        /// <summary>Transition to gameserver (client will authenticate and join/create game).</summary>
        ConnectingToGameserver,
        /// <summary>Connected to gameserver (going to auth and join game).</summary>
        ConnectedToGameserver,
        /// <summary>Joining game on gameserver.</summary>
        Joining,
        /// <summary>The client arrived inside a room. CurrentRoom and Players are known. Send events with OpRaiseEvent.</summary>
        Joined,
        /// <summary>Currently not used. Instead of OpLeave, the client disconnects from a server (which also triggers a leave immediately).</summary>
        Leaving,
        /// <summary>Currently not used.</summary>
        Left,
        /// <summary>Transition from gameserver to master (after leaving a room/game).</summary>
        DisconnectingFromGameserver,
        /// <summary>Currently not used.</summary>
        QueuedComingFromGameserver,
        /// <summary>The client disconnects (from any server).</summary>
        Disconnecting,
        /// <summary>The client is no longer connected (to any server). Connect to master to go on.</summary>
        Disconnected,
    }

    /// <summary>Ways a room ran be created or joined.</summary>
    public enum JoinType
    {
        CreateRoom,
        JoinRoom,
        JoinRandomRoom
    }

    #endregion

    /// <summary>
    /// This class extends the pure LoadBalancingPeer to implement the Photon LoadBalancing workflow.
    /// It keeps a state and will automatically execute transitions between the Master and Game Servers.
    /// </summary>
    /// <remarks>
    /// This class (and the Player class) should be extended to implement your own game logic.
    /// You can override CreatePlayer as "factory" method for Players and return your own Player instances.
    /// The State of this class is essential to know when a client is in a lobby (or just on the master)
    /// and when in a game where the actual gameplay should take place.
    /// Extension notes:
    /// An extension of this class should override the methods of the IPhotonPeerListener, as they 
    /// are called when the state changes. Call base.method first, then pick the operation or state you
    /// want to react to and put it in a switch-case.
    /// We try to provide demo to each platform where this api can be used, so lookout for those.
    /// </remarks>
    public class LoadBalancingClient : LoadBalancingPeer, IPhotonPeerListener
    {
        /// <summary>The version of your client. A new version also creates a new "virtual app" to separate players from older client versions.</summary>
        public string AppVersion { get; set; }

        /// <summary>The appName as assigned from the Photon Cloud or just the "regular" Photon Server Application Name ("LoadBalancing").</summary>
        public string AppId { get; set; }

        /// <summary>The master server's address. Defaults to "app.exitgamescloud.com:5055"</summary>
        public string MasterServerAddress { get; internal protected set; }

        /// <summary>The game server's address for a particular room. In use temporarily, as assigned by master.</summary>
        public string GameServerAddress { get; internal protected set; }

        /// <summary>Backing field for property.</summary>
        private ClientState state = ClientState.Uninitialized;

        /// <summary>Current state this client is in. Careful: several states are "transitions" that lead to other states.</summary>
        public ClientState State
        {
            get
            {
                return this.state;
            }

            protected internal set
            {
                this.state = value;
            }
        }

        /// <summary>Available server (types) for internally used field: server.</summary>
        private enum ServerConnection
        {
            MasterServer,
            GameServer
        }

        /// <summary>The server this client is currently connected or connecting to.</summary>
        private ServerConnection server;

        /// <summary>Backing field for property.</summary>
        private bool autoJoinLobby = true;

        /// <summary>If your client should join random games, you can skip joining the lobby. Call OpJoinRandomRoom and create a room if that fails.</summary>
        public bool AutoJoinLobby
        {
            get
            {
                return this.autoJoinLobby;
            }

            set
            {
                this.autoJoinLobby = value;
            }
        }

        /// <summary>
        /// Same as client.LocalPlayer.Name
        /// </summary>
        public string PlayerName
        {
            get
            {
                return this.LocalPlayer.Name;
            }

            set
            {
                if (this.LocalPlayer == null)
                {
                    return;
                }

                this.LocalPlayer.Name = value;
            }
        }

        /// <summary>This "list" is populated while being in the lobby of the Master. It contains RoomInfo per roomName (keys).</summary>
        public Dictionary<string, RoomInfo> RoomInfoList = new Dictionary<string, RoomInfo>();

        /// <summary>The current room this client is connected to (null if none available).</summary>
        public Room CurrentRoom;

        /// <summary>The local player is never null but not valid unless the client is in a room, too. The ID will be -1 outside of rooms.</summary>
        public Player LocalPlayer { get; set; }

        /// <summary>Statistic value available on master server: Players on master (looking for games).</summary>
        public int PlayersOnMasterCount { get; set; }

        /// <summary>Statistic value available on master server: Players in rooms (playing).</summary>
        public int PlayersInRoomsCount { get; set; }

        /// <summary>Statistic value available on master server: Rooms currently created.</summary>
        public int RoomsCount { get; set; }


        /// <summary>Internally used to decide if a room must be created or joined on game server.</summary>
        private JoinType lastJoinType;

        /// <summary>Internally used field to make identification of (multiple) clients possible.</summary>
        private static int clientCount;

        /// <summary>Internally used identification of clients. Useful to prefix debug output.</summary>
        private int clientId;

        public LoadBalancingClient() : base(ConnectionProtocol.Udp)
        {
            this.clientId = ++clientCount;
			if (clientId > 0) {}
            this.MasterServerAddress = "app.exitgamescloud.com:5055";
            this.Listener = this;
            this.LocalPlayer = this.CreatePlayer(string.Empty, -1, true, null);
        }

        public LoadBalancingClient(string masterAddress, string appId, string gameVersion) : this()
        {
            this.MasterServerAddress = masterAddress;
            this.AppId = appId;
            this.AppVersion = gameVersion;
        }

        #region Operations and Commands

        /// <summary>
        /// Starts the "process" to connect to the master server (initial connect).
        /// This includes connecting, establishing encryption, authentification and joining a lobby (if AutoJoinLobby is true).
        /// </summary>
        /// <param name="appId">Your application's name or ID assigned by Photon Cloud (webpage).</param>
        /// <param name="appVersion">The client's version (clients with differing client appVersions are separated and players don't meet).</param>
        /// <param name="playerName">This player's name.</param>
        /// <returns>If the operation could be send.</returns>
        public bool ConnectToMaster(string appId, string appVersion, string playerName)
        {
            this.AppId = appId;
            this.AppVersion = appVersion;
            this.PlayerName = playerName;

            if (base.Connect(this.MasterServerAddress, this.AppId))
            {
                this.State = ClientState.ConnectingToMasterserver;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Starts the "process" to connect to the master server (initial connect).
        /// This includes connecting, establishing encryption, authentification and joining a lobby (if AutoJoinLobby is true).
        /// </summary>
        public virtual bool Connect()
        {
            if (base.Connect(this.MasterServerAddress, this.AppId))
            {
                this.State = ClientState.ConnectingToMasterserver;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Starts the "process" to connect to the master server (initial connect).
        /// This includes connecting, establishing encryption, authentification and joining a lobby (if AutoJoinLobby is true).
        /// </summary>
        public override bool Connect(string serverAddress, string applicationName)
        {
            this.MasterServerAddress = serverAddress;
            this.AppId = applicationName;
            return this.Connect();
        }

        /// <summary>
        /// Starts the "process" to connect to the master server (initial connect).
        /// This includes connecting, establishing encryption, authentification and joining a lobby (if AutoJoinLobby is true).
        /// </summary>
        public override bool Connect(string serverAddress, string applicationName, byte node)
        {
            this.MasterServerAddress = serverAddress;
            this.AppId = applicationName;
            return this.Connect();
        }

        /// <summary>
        /// Disconnects this client from any server.
        /// </summary>
        public override void Disconnect()
        {
            this.State = ClientState.Disconnecting;
            base.Disconnect();
        }

        /// <summary>
        /// Leaves the CurrentRoom and returns to the Master server (back to the lobby).
        /// </summary>
        /// <remarks>
        /// This method actually is not an operation per se. It sets a state and calls Disconnect(). 
        /// This is is quicker than calling OpLeave and then disconnect (which also triggers a leave).
        /// </remarks>
        /// <returns>If the current room could be left (impossible while not in a room).</returns>
        public bool OpLeaveRoom()
        {
            if (this.CurrentRoom == null || !this.CurrentRoom.IsLocalClientInside)
            {
                return false;
            }

            this.DisconnectToReconnect();

            return true;
        }

        /// <summary>
        /// Internally used only.
        /// </summary>
        private void DisconnectToReconnect()
        {
            this.State = (this.server == ServerConnection.MasterServer)
                             ? ClientState.DisconnectingFromMasterserver
                             : ClientState.DisconnectingFromGameserver;
            base.Disconnect();
        }

        /// <summary>
        /// Internally used only.
        /// Starts the "process" to connect to the game server (connect before a game is joined).
        /// </summary>
        private bool ConnectToGameServer()
        {
            if (base.Connect(this.GameServerAddress, this.AppId))
            {
                this.State = ClientState.ConnectingToGameserver;
                return true;
            }

            // TODO: handle error "cant connect to GS"
            return false;
        }

        /// <summary>
        /// Operation to join a random, available room. 
        /// This operation fails if all rooms are closed or full.
        /// If successful, the result contains a gameserver address and the name of some room.
        /// </summary>
        /// <remarks>This override sets the state of the client.</remarks>
        /// <param name="expectedCustomRoomProperties">Optional. A room will only be joined, if it matches these custom properties (with string keys).</param>
        /// <param name="expectedMaxPlayers">Filters for a particular maxplayer setting. Use 0 to accept any maxPlayer value.</param>
        /// <returns>If the operation could be sent currently (requires connection).</returns>
        public override bool OpJoinRandomRoom(Hashtable expectedCustomRoomProperties, byte expectedMaxPlayers)
        {
            this.State = ClientState.Joining;
            this.lastJoinType = JoinType.JoinRandomRoom;
            this.CurrentRoom = new Room();

            return base.OpJoinRandomRoom(expectedCustomRoomProperties, expectedMaxPlayers);
        }

        public override bool OpJoinRandomRoom(Hashtable expectedCustomRoomProperties, byte expectedMaxPlayers, Hashtable playerProperties)
        {
            this.State = ClientState.Joining;
            this.lastJoinType = JoinType.JoinRandomRoom;
            this.CurrentRoom = new Room();

            return base.OpJoinRandomRoom(expectedCustomRoomProperties, expectedMaxPlayers, playerProperties);
        }

        /// <summary>
        /// Joins a room by name and sets this player's properties.
        /// </summary>
        /// <remarks>This override sets the state of the client.</remarks>
        /// <param name="roomName">The name of the room to join. Must be existing already, open and non-full or can't be joined.</param>
        /// <param name="playerProperties">Custom properties of "this player"  (use string-typed keys but short ones).</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public override bool OpJoinRoom(string roomName, Hashtable playerProperties)
        {
            this.State = ClientState.Joining;
            this.lastJoinType = JoinType.JoinRoom;
            this.CurrentRoom = new Room(roomName, null);
            this.LocalPlayer.CacheProperties(playerProperties);

            Hashtable playerPropsToSend = null;
            if (this.server == ServerConnection.GameServer)
            {
                playerPropsToSend = this.LocalPlayer.AllProperties;
            }

            return base.OpJoinRoom(roomName, playerPropsToSend);
        }

        /// <summary>
        /// Creates a room (on either Master or Game Server).
        /// The response depends on the server the peer is connected to: 
        /// Master will return a Game Server to connect to.
        /// Game Server will return the Room's data.
        /// This is an async request which triggers a OnOperationResponse() call.
        /// </summary>
        /// <remarks>This override sets the state of the client.</remarks>
        /// <param name="roomName">The name to create a room with. Must be unique and not in use or can't be created.</param>
        /// <param name="isVisible">Shows the room in the lobby's room list.</param>
        /// <param name="isOpen">Keeps players from joining the room (or opens it to everyone).</param>
        /// <param name="maxPlayers">Max players before room is considered full (but still listed).</param>
        /// <param name="customGameProperties">Custom properties to apply to the room on creation (use string-typed keys but short ones).</param>
        /// <param name="playerProperties">Custom properties of "this player"  (use string-typed keys but short ones).</param>
        /// <param name="propsListedInLobby">Defines the custom room properties that get listed in the lobby. Null defaults to "none", a string[0].</param>
        /// <returns>If the operation could be sent (has to be connected).</returns>
        public override bool OpCreateRoom(string roomName, bool isVisible, bool isOpen, byte maxPlayers, Hashtable customGameProperties, Hashtable playerProperties, string[] propsListedInLobby)
        {
            this.State = ClientState.Joining;
            this.lastJoinType = JoinType.CreateRoom;
            this.CurrentRoom = new Room(roomName, customGameProperties, isVisible, isOpen, maxPlayers, propsListedInLobby);
            this.LocalPlayer.CacheProperties(playerProperties);

            Hashtable playerPropsToSend = null;
            if (this.server == ServerConnection.GameServer)
            {
                playerPropsToSend = this.LocalPlayer.AllProperties;
            }

            return base.OpCreateRoom(roomName, isVisible, isOpen, maxPlayers, customGameProperties, playerPropsToSend, propsListedInLobby);
        }

        /// <summary>
        /// This updates the local cache of a player's properties before sending them to the server.
        /// Use this only when in state Joined.
        /// </summary>
        /// <param name="actorNr">ID of player to update/set properties for.</param>
        /// <param name="actorProperties">The properties to set for target actor.</param>
        /// <returns>If sending the properties to the server worked (not if the operation was executed successfully).</returns>
        protected internal override bool OpSetPropertiesOfActor(int actorNr, Hashtable actorProperties)
        {
            Player target = this.CurrentRoom.GetPlayer(actorNr);
            if (target != null)
            {
                target.CacheProperties(actorProperties);
            }

            return base.OpSetPropertiesOfActor(actorNr, actorProperties);
        }

        /// <summary>
        /// This updates the current room's properties before sending them to the server.
        /// Use this only when in state Joined.
        /// </summary>
        /// <param name="gameProperties">The roomProperties to udpate or set.</param>
        /// <returns>If sending the properties to the server worked (not if the operation was executed successfully).</returns>
        protected internal override bool OpSetPropertiesOfRoom(Hashtable gameProperties)
        {
            this.CurrentRoom.CacheProperties(gameProperties);
            return base.OpSetPropertiesOfRoom(gameProperties);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Internally used only.
        /// Reads out properties coming from the server in events and operation responses (which might be a bit tricky).
        /// </summary>
        private void ReadoutProperties(Hashtable gameProperties, Hashtable actorProperties, int targetActorNr)
        {
            // Debug.LogWarning("ReadoutProperties game=" + gameProperties + " actors(" + actorProperties + ")=" + actorProperties + " " + targetActorNr);
            // read game properties and cache them locally
            if (this.CurrentRoom != null && gameProperties != null)
            {
                this.CurrentRoom.CacheProperties(gameProperties);
            }

            if (actorProperties != null && actorProperties.Count > 0)
            {
                if (targetActorNr > 0)
                {
                    // we have a single entry in the actorProperties with one user's name
                    // targets MUST exist before you set properties
                    Player target = this.CurrentRoom.GetPlayer(targetActorNr);
                    if (target != null)
                    {
                        target.CacheProperties(this.ReadoutPropertiesForActorNr(actorProperties, targetActorNr));
                    }
                }
                else
                {
                    // in this case, we've got a key-value pair per actor (each
                    // value is a hashtable with the actor's properties then)
                    int actorNr;
                    Hashtable props;
                    string newName;
                    Player target;

                    foreach (object key in actorProperties.Keys)
                    {
                        actorNr = (int)key;
                        props = (Hashtable)actorProperties[key];
                        newName = (string)props[ActorProperties.PlayerName];

                        target = this.CurrentRoom.GetPlayer(actorNr);
                        if (target == null)
                        {
                            target = this.CreatePlayer(newName, actorNr, false, props);
                            this.CurrentRoom.StorePlayer(target);
                        }
                        else
                        {
                            target.CacheProperties(props);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Internally used only to read properties for a distinct actor (which might be the hashtable OR a key-pair value IN the actorProperties).
        /// </summary>
        private Hashtable ReadoutPropertiesForActorNr(Hashtable actorProperties, int actorNr)
        {
            if (actorProperties.ContainsKey(actorNr))
            {
                return (Hashtable)actorProperties[actorNr];
            }

            return actorProperties;
        }

        /// <summary>
        /// Internally used to set the LocalPlayer's ID.
        /// </summary>
        /// <param name="newID">New actor ID (a.k.a actorNr) assigned when joining a room.</param>
        protected internal void ChangeLocalID(int newID)
        {
            if (this.LocalPlayer == null)
            {
                this.DebugReturn(DebugLevel.WARNING, string.Format("Local actor is null or not in mActors! mLocalActor: {0} mActors==null: {1} newID: {2}", this.LocalPlayer, this.CurrentRoom.Players == null, newID));
            }

            if (this.CurrentRoom == null)
            {
                // change to new actor/player ID and make sure the player does not have a room reference left
                this.LocalPlayer.ChangeLocalID(newID);
                this.LocalPlayer.RoomReference = null;
            }
            else
            {
                // remove old actorId from actor list
                this.CurrentRoom.RemovePlayer(this.LocalPlayer);

                // change to new actor/player ID
                this.LocalPlayer.ChangeLocalID(newID);

                // update the room's list with the new reference
                this.CurrentRoom.StorePlayer(this.LocalPlayer);

                // make this client known to the local player (used to get state and to sync values from within Player)
                this.LocalPlayer.LoadBalancingClient = this;
            }
        }

        /// <summary>
        /// Internally used to clean up local instances of players and room.
        /// </summary>
        private void CleanCachedValues()
        {
            this.ChangeLocalID(-1);

            // if this is called on the gameserver, we clean the room we were in. on the master, we keep the room to get into it
            if (this.server == ServerConnection.GameServer)
            {
                this.CurrentRoom = null;    // players get cleaned up inside this, too, except LocalPlayer (which we keep)
            }

            // when we leave the master, we clean up the rooms list (which might be updated by the lobby when we join again)
            if (this.server == ServerConnection.MasterServer)
            {
                this.RoomInfoList.Clear();
            }
        }

        /// <summary>
        /// Called internally, when a game was joined or created on the game server. 
        /// This reads the response, finds out the local player's actorNumber (a.k.a. Player.ID) and applies properties of the room and players.
        /// </summary>
        /// <param name="operationResponse"></param>
        private void GameEnteredOnGameServer(OperationResponse operationResponse)
        {
            if (operationResponse.ReturnCode != 0)
            {
                switch (operationResponse.OperationCode)
                {
                    case OperationCode.CreateGame:
                        this.DebugReturn(DebugLevel.ERROR, "Create failed on GameServer. Changing back to MasterServer.");
                        break;
                    case OperationCode.JoinGame:
                    case OperationCode.JoinRandomGame:
                        this.DebugReturn(DebugLevel.ERROR, "Join failed on GameServer. Changing back to MasterServer.");

                        if (operationResponse.ReturnCode == ErrorCode.GameDoesNotExist)
                        {
                            this.DebugReturn(DebugLevel.INFO, "Most likely the game became empty during the switch to GameServer.");
                        }

                        // TODO: add callback to join failed
                        break;
                }

                this.DisconnectToReconnect();
                return;
            }

            this.State = ClientState.Joined;
            this.CurrentRoom.LoadBalancingClient = this;
            this.CurrentRoom.IsLocalClientInside = true;

            // the local player's actor-properties are not returned in join-result. add this player to the list
            int localActorNr = (int)operationResponse[ParameterCode.ActorNr];
            this.ChangeLocalID(localActorNr);

            Hashtable actorProperties = (Hashtable)operationResponse[ParameterCode.PlayerProperties];
            Hashtable gameProperties = (Hashtable)operationResponse[ParameterCode.GameProperties];
            this.ReadoutProperties(gameProperties, actorProperties, 0);

            switch (operationResponse.OperationCode)
            {
                case OperationCode.CreateGame:
                    // TODO: add callback "game created"
                    break;
                case OperationCode.JoinGame:
                case OperationCode.JoinRandomGame:
                    // TODO: add callback "game joined"
                    break;
            }
        }

        /// <summary>
        /// Internally used "factory" method to create a player. 
        /// Override this method to replace Player with some extended class (and provide your own, game-specific values in there).
        /// </summary>
        /// <param name="actorName"></param>
        /// <param name="actorNumber"></param>
        /// <param name="isLocal"></param>
        /// <param name="actorProperties"></param>
        /// <returns></returns>
        protected internal virtual Player CreatePlayer(string actorName, int actorNumber, bool isLocal, Hashtable actorProperties)
        {
            Player newPlayer = new Player(actorName, actorNumber, isLocal);
            newPlayer.CacheProperties(actorProperties);
            return newPlayer;
        }

        #endregion

        #region IPhotonPeerListener

        /// <summary>
        /// Debug output of low level api (and this client).
        /// </summary>
        public virtual void DebugReturn(DebugLevel level, string message)
        {
			UnityEngine.Debug.Log(message);
            Console.Out.WriteLine(message);
        }

        /// <summary>
        /// Uses the operationResponse's provided by the server to advance the internal state and call ops as needed.
        /// </summary>
        public virtual void OnOperationResponse(OperationResponse operationResponse)
        {
            switch (operationResponse.OperationCode)
            {
                case OperationCode.Authenticate:
                    {
                        if (operationResponse.ReturnCode != 0)
                        {
                            this.State = ClientState.Disconnecting;
                            this.Disconnect();
                            break;
                        }

                        if (this.State == ClientState.ConnectedToMaster)
                        {
                            this.State = ClientState.Authenticated;
                            if (this.AutoJoinLobby)
                            {
                                this.OpJoinLobby();
                            }
                        }
                        else if (this.State == ClientState.ConnectedToGameserver)
                        {
                            this.State = ClientState.Joining;
                            if (this.lastJoinType == JoinType.JoinRoom || this.lastJoinType == JoinType.JoinRandomRoom)
                            {
                                // if we just "join" the game, do so
                                this.OpJoinRoom(this.CurrentRoom.Name, this.LocalPlayer.CustomProperties);
                            }
                            else if (this.lastJoinType == JoinType.CreateRoom)
                            {
                                this.OpCreateRoom(
                                    this.CurrentRoom.Name,
                                    this.CurrentRoom.IsVisible,
                                    this.CurrentRoom.IsOpen,
                                    this.CurrentRoom.MaxPlayers,
                                    this.CurrentRoom.CustomProperties,
                                    this.LocalPlayer.CustomProperties,
                                    this.CurrentRoom.PropsListedInLobby);
                            }
                            break;
                        }
                        break;
                    }

                case OperationCode.Leave:
                    this.CleanCachedValues();
                    break;

                case OperationCode.JoinLobby:
                    this.State = ClientState.JoinedLobby;
                    break;

                case OperationCode.JoinRandomGame:  // this happens only on the master server. on gameserver this is a "regular" join
                case OperationCode.CreateGame:
                case OperationCode.JoinGame:
                    {
                        if (this.server == ServerConnection.GameServer)
                        {
                            this.GameEnteredOnGameServer(operationResponse);
                        }
                        else
                        {
                            if (operationResponse.ReturnCode == ErrorCode.NoRandomMatchFound)
                            {
                                // this happens only for JoinRandomRoom
                                // TODO: implement callback/reaction when no random game could be found (this is no bug and can simply happen if no games are open)
                                this.state = ClientState.JoinedLobby; // TODO: maybe we have to return to another state here (if we didn't join a lobby)
                                break;
                            }

                            // TODO: handle more error cases
                            if (operationResponse.ReturnCode != 0)
                            {
                                if (this.DebugOut >= DebugLevel.ERROR)
                                {
                                    this.DebugReturn(DebugLevel.ERROR, string.Format("Getting into game failed, client stays on masterserver: {0}.", operationResponse.ToStringFull()));
                                }

                                this.state = ClientState.JoinedLobby; // TODO: maybe we have to return to another state here (if we didn't join a lobby)
                                break;
                            }

                            this.GameServerAddress = (string)operationResponse[ParameterCode.Address];
                            string gameId = operationResponse[ParameterCode.RoomName] as string;
                            if (!string.IsNullOrEmpty(gameId))
                            {
                                // is only sent by the server's response, if it has not been sent with the client's request before!
                                this.CurrentRoom.Name = gameId;
                            }

                            this.DisconnectToReconnect();
                        }

                        break;
                    }
            }
        }

        /// <summary>
        /// Uses the connection's statusCodes to advance the internal state and call ops as needed.
        /// </summary>
        public virtual void OnStatusChanged(StatusCode statusCode)
        {
            switch (statusCode)
            {
                case StatusCode.Connect:
                    if (this.State == ClientState.ConnectingToGameserver)
                    {
                        if (this.DebugOut >= DebugLevel.ALL)
                        {
                            this.DebugReturn(DebugLevel.ALL, "Connected to gameserver.");
                        }

                        this.State = ClientState.ConnectedToGameserver;
                        this.server = ServerConnection.GameServer;
                    }

                    if (this.State == ClientState.ConnectingToMasterserver)
                    {
                        if (this.DebugOut >= DebugLevel.ALL)
                        {
                            this.DebugReturn(DebugLevel.ALL, "Connected to masterserver.");
                        }

                        this.State = ClientState.ConnectedToMaster;
                        this.server = ServerConnection.MasterServer;
                    }

                    this.EstablishEncryption();
                    break;

                case StatusCode.ExceptionOnConnect:
                    this.State = ClientState.Disconnected;
                    break;

                case StatusCode.Disconnect:
                    // TODO: handle disconnect due to connection exception (don't connect to GS or master in that case)

                    this.CleanCachedValues();

                    if (this.State == ClientState.Disconnecting)
                    {
                        this.State = ClientState.Disconnected;
                    }
                    else if (this.State == ClientState.Uninitialized)
                    {
                        this.State = ClientState.Disconnected;
                    }
                    else if (this.State != ClientState.Disconnected)
                    {
                        if (this.server == ServerConnection.GameServer)
                        {
                            this.ConnectToMaster(this.AppId, this.AppVersion, this.PlayerName);
                        }
                        else if (this.server == ServerConnection.MasterServer)
                        {
                            this.ConnectToGameServer();
                        }
                    }
                    break;

                case StatusCode.EncryptionEstablished:
                    if (!this.OpAuthenticate(this.AppId, this.AppVersion))
                    {
                        this.DebugReturn(DebugLevel.ERROR, "Error Authenticating! Did not work.");
                    }

                    break;

                case StatusCode.Exception:
                    this.State = ClientState.Disconnected;
                    break;
            }
        }

        /// <summary>
        /// Uses the photonEvent's provided by the server to advance the internal state and call ops as needed.
        /// </summary>
        public virtual void OnEvent(EventData photonEvent)
        {
            switch (photonEvent.Code)
            {
                case EventCode.GameList:
                case EventCode.GameListUpdate:
                    if (photonEvent.Code == EventCode.GameList)
                    {
                        this.RoomInfoList = new Dictionary<string, RoomInfo>();
                    }

                    Hashtable games = (Hashtable)photonEvent[ParameterCode.GameList];
                    foreach (string gameName in games.Keys)
                    {
                        RoomInfo game = new RoomInfo(gameName, (Hashtable)games[gameName]);
                        if (game.removedFromList)
                        {
                            this.RoomInfoList.Remove(gameName);
                        }
                        else
                        {
                            this.RoomInfoList[gameName] = game;
                        }
                    }
                    break;

                case EventCode.Join:
                    Hashtable actorProperties = (Hashtable)photonEvent[ParameterCode.PlayerProperties];
                    string actorName = (string)actorProperties[ActorProperties.PlayerName];
                    int actorNr = (int)photonEvent[ParameterCode.ActorNr];  // actorNr (a.k.a. playerNumber / ID) of sending player
                    bool isLocal = this.LocalPlayer.ID == actorNr;

                    if (!isLocal)
                    {
                        Player newPlayer = this.CreatePlayer(actorName, actorNr, isLocal, actorProperties);
                        this.CurrentRoom.StorePlayer(newPlayer);
                    }
                    break;

                case EventCode.Leave:
                    int actorID = (int)photonEvent[ParameterCode.ActorNr];
                    this.CurrentRoom.RemovePlayer(actorID);
                    break;

                case EventCode.PropertiesChanged:
                    // whenever properties are sent in-room, they can be broadcasted as event (which we handle here)
                    // we get PLAYERproperties if actorNr > 0 or ROOMproperties if actorNumber is not set or 0
                    int targetActorNr = 0;
                    if (photonEvent.Parameters.ContainsKey(ParameterCode.TargetActorNr))
                    {
                        targetActorNr = (int)photonEvent[ParameterCode.TargetActorNr];
                    }
                    Hashtable props = (Hashtable)photonEvent[ParameterCode.Properties];

                    if (targetActorNr > 0)
                    {
                        this.ReadoutProperties(null, props, targetActorNr);
                    }
                    else
                    {
                        this.ReadoutProperties(props, null, 0);
                    }

                    break;

                case EventCode.AppStats:
                    // only the master server sends these in (1 minute) intervals
                    this.PlayersInRoomsCount = (int)photonEvent[ParameterCode.PeerCount];
                    this.RoomsCount = (int)photonEvent[ParameterCode.GameCount];
                    this.PlayersOnMasterCount = (int)photonEvent[ParameterCode.MasterPeerCount];
                    break;
            }
        }


        #endregion
    }
}
