
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Enums;
    using ExitGames.Client.Photon;

    public static class Constants
    {
		  //internal const byte EV_PLAYER_INFO = 104;
        internal const byte EV_MOVE = 101;
        internal const byte EV_ANIM = 102;
        internal const byte EV_FIRE = 103;
        internal const byte EV_CHAT = 105;
		internal const byte EV_HIT = 106;
		internal const byte EV_FRAG = 107;        
	
		internal const byte EV_HARPOON = 130;
		internal const byte EV_HARPOON_HIT = 131;
		internal const byte EV_DEPTHCHARGE = 132;
		internal const byte EV_DEPTHCHARGE_HIT = 133;
		internal const byte EV_BUBBLES = 134;
		internal const byte EV_SILTCLOUD = 135;
		//todo -- global fog level?

        internal const byte STATUS_PLAYER_POS = 43;
        internal const byte STATUS_TARGET_POS = 45;
        internal const byte STATUS_PLAYER_ROT = 46;

        internal const byte STATUS_PLAYER_KEYSTATE = 49;
        //internal const byte STATUS_PLAYER_NAME = 47;
        //internal const byte STATUS_PLAYER_COLOR = 48;
        internal const byte STATUS_PLAYER_AIM = 50;
        //internal const byte STATUS_PLAYER_CROUCH = 51;
        internal const byte STATUS_PLAYER_INAIR = 52;
        internal const byte STATUS_PLAYER_FIRE = 53;

        internal const byte STATUS_PLAYER_GUN = 54;
        internal const byte STATUS_PLAYER_POINTX = 55;
        internal const byte STATUS_PLAYER_POINTY = 56;
        internal const byte STATUS_PLAYER_POINTZ = 57;

        internal const byte STATUS_PLAYER_OUTPUTPOINTX = 61;
        internal const byte STATUS_PLAYER_OUTPUTPOINTY = 62;
        internal const byte STATUS_PLAYER_OUTPUTPOINTZ = 63;
	
		internal const byte STATUS_STARTPOS_X = 30;
		internal const byte STATUS_STARTPOS_Y = 31;
		internal const byte STATUS_STARTPOS_Z = 32;
	
		internal const byte STATUS_STARTROT_X = 33;
		internal const byte STATUS_STARTROT_Y = 34;
		internal const byte STATUS_STARTROT_Z = 35;
	
		internal const byte STATUS_STARTTIME = 36;
	
		internal const byte STATUS_ENDPOS_X = 37;
		internal const byte STATUS_ENDPOS_Y = 38;
		internal const byte STATUS_ENDPOS_Z = 39;
	
		internal const byte STATUS_ENDTIME = 40;
		internal const byte STATUS_COUNT = 41;
	
	
	

    }
