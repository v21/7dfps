// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Game.cs" company="Exit Games GmbH">
//   Copyright (c) Exit Games GmbH.  All rights reserved.
// </copyright>
// <summary>
//   The Game class wraps up usage of a PhotonPeer, event handling and simple game logic. To make it work,
//   it must be integrated in a "gameloop", which regularly calls Game.Update().
//   A PhotonPeer is not thread safe, so make sure Update() is only called by one thread.
//
//   This sample should show how to get a player's position across to other players in the same room.
//   Each running instance will connect to Photon (with a local player / peer), go into the same room and
//   move around.
//   Players have positions (updated regularly), name and color (updated only when someone joins).
//   Server side, Photon with the default Lite Application is used.
//   This class encapsulates the (simple!) logic for the Realtime Demo in a way that can be used on several
//   DotNet platforms (DotNet, Unity3D and Silverlight).
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;

using ExitGames.Client.Photon.LoadBalancing;
using ExitGames.Client.Photon;

public class Game : LoadBalancingClient
{
    #region Members

    public PlayerLocal PlayerLocal; // references the 3D model of the player

    public ChatScreen chatScreen;

    public delegate void DebugOutputDelegate(string debug);

    public DebugOutputDelegate DebugListeners;
	
	public static Game instance;
	
    private readonly usePhoton usePhoton;

    #endregion

    #region Constructor and Gameloop

    public Game(DebugOutputDelegate debugDelegate, usePhoton photon)
        : base()
    {
        this.usePhoton = photon;
        this.DebugListeners = debugDelegate;
		instance = this;
    }

    /// <summary>
    /// Update must be called by a gameloop (a single thread), so it can handle
    /// automatic movement and networking.
    /// </summary>
    public void Update()
    {
        this.Service();
    }

    #endregion

    #region IPhotonPeerListener Members

    private void SendPlayerInfo()
    {
        var data = this.PlayerLocal.GetProperties();
        this.OpRaiseEvent(Player.EV_PLAYER_INFO, data, false, 0);
    }

    public override void OnEvent(EventData eventData)
    {
        // not all events have a actorNumber. if there's none, we most likely don't need it and -1 indicates this
        int actorNr = -1;
        if (eventData.Parameters.ContainsKey(ParameterCode.ActorNr))
        {
            actorNr = (int)eventData[ParameterCode.ActorNr];
        }

        // this player is potentially no longer in the players list, after base.OnEvent(). take care  :)
        Player playerOfEvent = null;
        if (this.CurrentRoom != null)
        {
            playerOfEvent = this.CurrentRoom.GetPlayer(actorNr);
        }

        // let the LoadbalancingClient handle it's players and room, etc.
        base.OnEvent(eventData);

        // if the player wasn't in the players list before, it might be now, after base.OnEvent(). check again.
        if (playerOfEvent == null && this.CurrentRoom != null)
        {
            playerOfEvent = this.CurrentRoom.GetPlayer(actorNr);
        }

        switch (eventData.Code)
        {
            case EventCode.Join:
                {
                    if (playerOfEvent.playerRemote == null && !playerOfEvent.IsLocal)
                    {
						usePhoton.AddRemotePlayer(playerOfEvent);
                        var properties = (Hashtable)eventData[ParameterCode.PlayerProperties];
                        playerOfEvent.playerRemote.SetProperties(properties);
                    }

                    // send my player information
                    this.SendPlayerInfo();

                    this.PrintPlayers();
                }
                break;

            case EventCode.Leave:
                {
                    this.usePhoton.RemoveRemotePlayer(playerOfEvent);
                }
                break;

            case Player.EV_PLAYER_INFO:
                {
                    if (playerOfEvent != null)
                    {
                        playerOfEvent.playerRemote.SetProperties((Hashtable)eventData[ParameterCode.Data]);
                    }
                }
                break;

            case Constants.EV_HIT:
                {
                    if (playerOfEvent != null)
                    {
                        Hashtable data = (Hashtable)eventData[(byte)ParameterCode.Data];
                        int target = (int)data["T"];
                        if (target == this.LocalPlayer.ID)
                        {
                            switch ((byte)data["H"])
                            {
                                case 0:
                                    PlayerLocal.DownLifeHitSoldier(playerOfEvent.playerRemote.Name);
                                    break;
                                case 1:
                                    PlayerLocal.DownLifeHitSoldierGranade(playerOfEvent.playerRemote.Name);
                                    break;
                            }
                        }
                    }
                }
                break;

            case Constants.EV_MOVE:
                {
                    if (playerOfEvent != null)
                    {
                        if (playerOfEvent.playerRemote != null) playerOfEvent.playerRemote.SetPosition((Hashtable)eventData[(byte)ParameterCode.Data]);
                    }
                }
                break;

            case Constants.EV_ANIM:
                {
                    if (playerOfEvent != null)
                    {
                        playerOfEvent.playerRemote.SetAnim((Hashtable)eventData[(byte)ParameterCode.Data]);
                    }
                }
                break;

            case Constants.EV_FRAG:
                {
                    playerOfEvent = this.CurrentRoom.GetPlayer(actorNr);
                    if (playerOfEvent != null)
                    {
                        playerOfEvent.playerRemote.Dead = true;
                        playerOfEvent.playerRemote.SetSpawnPosition((Hashtable)eventData[(byte)ParameterCode.Data]);
                    }
                }
                break;

            case Constants.EV_FIRE:
                {
                    actorNr = (int)eventData[ParameterCode.ActorNr];
                    playerOfEvent = this.CurrentRoom.GetPlayer(actorNr);
                    if (playerOfEvent != null)
                    {
                        playerOfEvent.playerRemote.SetFire((Hashtable)eventData[(byte)ParameterCode.Data]);
                    }
                }
                break;
			case Constants.EV_HARPOON:
				{
					if (playerOfEvent != null)
					{
						playerOfEvent.playerRemote.RemoteFireHarpoon((Hashtable)eventData[(byte)ParameterCode.Data]);
					}
				}	
				break;
			case Constants.EV_HARPOON_HIT:
				{
					if (playerOfEvent != null)
					{
						playerOfEvent.playerRemote.RemoteHarpoonHit((Hashtable)eventData[(byte)ParameterCode.Data]);
					}
				}	
				break;
			case Constants.EV_DEPTHCHARGE:
				{
					if (playerOfEvent != null)
					{
						playerOfEvent.playerRemote.RemoteFireDepthCharge((Hashtable)eventData[(byte)ParameterCode.Data]);
					}
				}	
				break;
			case Constants.EV_DEPTHCHARGE_HIT:
				{
					if (playerOfEvent != null)
					{
						playerOfEvent.playerRemote.RemoteDepthChargeHit((Hashtable)eventData[(byte)ParameterCode.Data]);
					}
				}	
				break;
			case Constants.EV_SILTCLOUD:
				{
					usePhoton.RemoteSpawnSiltcloud((Hashtable)eventData[(byte)ParameterCode.Data]);
					
				}	
				break;
			case Constants.EV_BUBBLES:
				{
					if (playerOfEvent != null)
					{
						playerOfEvent.playerRemote.RemoteBreathe((Hashtable)eventData[(byte)ParameterCode.Data]);
					}
				}	
				break;
            case Constants.EV_CHAT:
                {
                    chatScreen.IncomingMessage((Hashtable)eventData[(byte)ParameterCode.Data]);
                }
                break;

        }
    }

    #endregion
	
	
	
	
    #region Game Handling

    public override void DebugReturn(DebugLevel level, string message)
    {
        base.DebugReturn(level, message);
        this.usePhoton.DebugReturn(message);
    }

    public override void OnOperationResponse(OperationResponse operationResponse)
    {
        base.OnOperationResponse(operationResponse);

        switch (operationResponse.OperationCode)
        {
            case OperationCode.Authenticate:
                {
                    if (operationResponse.ReturnCode == ErrorCode.InvalidAuthentication)
                    {
                        this.usePhoton.ToggleAppIdWasNotAuthenticated();
                    }
                    break;
                }

            case OperationCode.JoinRandomGame:
            case OperationCode.CreateGame:
            case OperationCode.JoinGame:
                {
                    if (operationResponse[ParameterCode.Address] != null)
                    {
                        var gameServerAddress = (string)operationResponse[ParameterCode.Address];
                        string[] parts = gameServerAddress.Split(':');
                        if (parts.Length == 2 && parts[0].Equals("127.0.0.1"))
                        {
                            if (this.usePhoton == null)
                            {
                                DebugReturn("usePhoton is null...");
                            }
                            else
                            {
                                this.usePhoton.SendMessage("ToggleGameServerAddressIsLocalhost");
                            }
                            break;
                        }
                    }

                    if (this.State == ClientState.Joined)
                    {
                        int actorNrReturnedForOpJoin = (int)operationResponse[ParameterCode.ActorNr];

                        foreach (Player oldPlayer in this.CurrentRoom.Players.Values)
                        {
                            if (oldPlayer.IsLocal == false)
                            {
                                usePhoton.RemoveRemotePlayer(oldPlayer);
                            }
                        }

                        foreach (KeyValuePair<int, Player> player in this.CurrentRoom.Players)
                        {
                            if (((Player)player.Value).ID == actorNrReturnedForOpJoin)
                            {
                                continue;
                            }

                            this.usePhoton.AddRemotePlayer((Player)player.Value);
                            var props = operationResponse[ParameterCode.PlayerProperties];
                            ((Player)player.Value).playerRemote.SetProperties((Hashtable)props);
                        }
                    }

                    this.usePhoton.GameManager.SendMessage("initGame");
                    break;
                }
        }
    }

    public void JoinRoom(string room)
    {
        if (!this.usePhoton.Player.gameObject.GetComponent<PlayerLocal>())
        {
            // create a player Transform instance 
            this.PlayerLocal = this.usePhoton.Player.gameObject.AddComponent<PlayerLocal>();
			this.usePhoton.PlaceLocalPlayer(this.usePhoton.Player);
            this.PlayerLocal.Initialize(this);
            this.PlayerLocal.name = this.LocalPlayer.Name + this.LocalPlayer.ID;

            this.chatScreen = this.usePhoton.chatScreen;
            this.chatScreen.NameSender = this.LocalPlayer.Name;
        }

        Hashtable properties = this.PlayerLocal.GetProperties();
        this.OpJoinRoom(room, properties);

        this.chatScreen.Initialize(this);
    }

    // Simple "help" function to print current list of players.
    // As this function uses the players list, make sure it's not called while 
    // peer.DispatchIncomingCommands() might modify the list!! (e.g. by lock(this))
    public void PrintPlayers()
    {
        if (this.CurrentRoom == null) return;
        string players = "Players: ";
        foreach (Player p in this.CurrentRoom.Players.Values)
        {
            players += p.ToString() + ", ";
        }

        this.DebugReturn(players);
    }

    // This is only used by the game / application, not by the Photon library
    public void DebugReturn(string debug)
    {
        this.DebugListeners(debug);
    }

    #endregion
}
