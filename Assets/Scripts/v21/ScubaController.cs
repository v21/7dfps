using UnityEngine;
using System.Collections;

public class ScubaController : MonoBehaviour {
	
	private bool local;
	public int ActorNr;
	public string ActorName;
	public bool DisplayKillYou;
	
	public float pitch;
	public float yaw;
	public float roll;
	
	public float pitchMultiplier;
	public float yawMultiplier;
	public float rollMultiplier;
	public float rollLevelling;
	
	public float weaknessModifier = 1;
	
	public float speed;
	public float speedScale;
	public float targetSpeed;
	private float speedDampVelocity = 0f;
	public float speedDampTime = 0.3f;
	
	private float lastPressTime;
	public float dragImmunityTime;
	
	public AnimationCurve InputToMovement;
	
	public AnimationCurve ForwardsDownSpeedInc;
	public AnimationCurve ForwardsSpeedInc;
	public AnimationCurve BackwardsDownSpeedInc;
	public AnimationCurve BackwardsSpeedInc;
	public AnimationCurve NoneSpeedInc;
	
	public AnimationCurve speedSteerCurve;
	
	// Use this for initialization
	void Start () {
	
	}
	
	public void SetLocal() {
		local = true;	
		
	}
	
	public void SetRemote() {
		local = false;	
		
	}
	
	void FixedUpdate () {
		
		if (local) {
			
						
			pitch = Input.GetAxis ("Mouse Y") * pitchMultiplier * speedSteerCurve.Evaluate(targetSpeed); ///todo : daaamp it
			yaw = Input.GetAxis ("Mouse X") * yawMultiplier * speedSteerCurve.Evaluate(targetSpeed);
			roll = Input.GetAxis ("Mouse X") * rollMultiplier * speedSteerCurve.Evaluate(targetSpeed);
			
		
			
			//get current pitch and roll
			float currPitch = transform.rotation.eulerAngles.x;
			if (currPitch > 180) {
				currPitch = currPitch - 360; //convert to -180 to 180
			}
			
			//float currYaw = transform.rotation.eulerAngles.y;
			
			
			float currRoll = transform.rotation.eulerAngles.z;
			if (currRoll > 180) {
				currRoll = currRoll - 360; //convert to -180 to 180
			}
			
			//don't level off a roll if we're going straight up or down
			if (60 > Mathf.Abs (currPitch)) {
				roll -= rollLevelling * currRoll;
			}
			
			
			
			rigidbody.rotation *= Quaternion.Euler(pitch, yaw, roll);
			//transform.Rotate(pitch, yaw, roll);
			
			
		
			
			
			
			
			//todo: some cooloff to the forwards down extra impulse.
			
			if (Input.GetButtonDown("Forwards")) {
				lastPressTime = Time.time;
				targetSpeed += ForwardsDownSpeedInc.Evaluate(targetSpeed) * Time.deltaTime;
			}
			else if (Input.GetButton("Forwards")) {
				targetSpeed += ForwardsSpeedInc.Evaluate(targetSpeed) * Time.deltaTime;
			}
			else if (Input.GetButtonDown("Backwards")) {
				
				lastPressTime = Time.time;
				targetSpeed += BackwardsDownSpeedInc.Evaluate(targetSpeed) * Time.deltaTime;
			}
			else if (Input.GetButton("Backwards")) {
				targetSpeed += BackwardsSpeedInc.Evaluate(targetSpeed);
			}
			else {
				if (lastPressTime < Time.time - dragImmunityTime){
					targetSpeed += NoneSpeedInc.Evaluate(targetSpeed) * Time.deltaTime;
				}
			}
			
			speed = Mathf.SmoothDamp(speed, targetSpeed * speedScale * weaknessModifier, ref speedDampVelocity, speedDampTime);
			
			//rigidbody.velocity =  transform.TransformDirection(0f,0f,1f)
			gameObject.transform.Translate(new Vector3(0, 0, speed));
			
			Vector3 forwardVector = Vector3.forward * speed;
			rigidbody.velocity = rigidbody.rotation * forwardVector;
			
			
		}
		else {
			rigidbody.velocity = Vector3.zero;
			rigidbody.angularVelocity = Vector3.zero;
		}
	}
	
	
	
	void SetActorNr(Hashtable DataActor)
	{
	    ActorNr = (int)DataActor["actornr"];
	    ActorName = (string)DataActor["actorname"];
	    /*if(actorText == null && gameObject.layer != 13)
	    {
            this.actorText = Instantiate(Resources.Load("ActorName"));
		    this.actorText.name = ActorNr+"";
            actorText.transform.localScale = new Vector3(1.4 / 8f, 1.4 / 8f, 1.4 / 8f);
        }*/
        
	}
	
	
	void OnCollisionEnter (Collision collision) {
		Debug.Log("Scuba Controller : Collision!", this);	
		//todo : temp loss of control, while we bounce off
		speed = 0f; // speed * collisionFactor;
		targetSpeed = 0f;
		//todo:sharp exhale	
	}
	
	
	
	void OnTriggerEnter (Collider other) {
		
		
		
		Debug.Log("trigger enter " + other.tag);
		
	}
	
	
	void OnTriggerExit (Collider other) {
		
	}
}
