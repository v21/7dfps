using UnityEngine;
using System.Collections;

public class HarpoonGun : MonoBehaviour {
	
	private bool local = false;
	
	public GameObject harpoonPrefab;
	
	public Transform harpoonStart;
	
	public float cooldownTime;
	private float lastFiredTime;
	
	public float harpoonSpeed;
	public static float _harpoonSpeed;
	
	private Transform loadedHarpoon;
	
	// Use this for initialization
	void Start () {
		_harpoonSpeed = harpoonSpeed;
		loadedHarpoon = ((GameObject)Instantiate(harpoonPrefab, harpoonStart.position, harpoonStart.rotation)).transform;
		loadedHarpoon.parent = transform;
		
		if (local) loadedHarpoon.SendMessage("SetLocal");
		else loadedHarpoon.SendMessage("SetRemote");
	}
	
	void SetLocal() {
		local = true;	
	}
	
	// Update is called once per frame
	void Update () {
		if (lastFiredTime + cooldownTime < Time.time) {
			if (loadedHarpoon == null) {
				loadedHarpoon = ((GameObject)Instantiate(harpoonPrefab, harpoonStart.position, harpoonStart.rotation)).transform;
				loadedHarpoon.parent = transform;
				
				if (local) loadedHarpoon.SendMessage("SetLocal");
				else loadedHarpoon.SendMessage("SetRemote");
			}
		}
		if (local) {
			if (Input.GetButtonDown("Fire1") && !ChatScreen.chat) {
				if (loadedHarpoon != null) {
					
					Hashtable h = new Hashtable();
					
					h.Add (Constants.STATUS_STARTPOS_X, harpoonStart.position.x);
					h.Add (Constants.STATUS_STARTPOS_Y, harpoonStart.position.y);
					h.Add (Constants.STATUS_STARTPOS_Z, harpoonStart.position.z);
					
					h.Add (Constants.STATUS_STARTROT_X, harpoonStart.rotation.eulerAngles.x);
					h.Add (Constants.STATUS_STARTROT_Y, harpoonStart.rotation.eulerAngles.y);
					h.Add (Constants.STATUS_STARTROT_Z, harpoonStart.rotation.eulerAngles.z);
					
					h.Add(Constants.STATUS_STARTTIME, Game.instance.ServerTimeInMilliSeconds);
					FireHarpoon(h);
					transform.SendMessage("LocalFireHarpoon", h, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}
	
	void FireHarpoon (Hashtable h) {
		lastFiredTime = Time.time;
		if (loadedHarpoon == null)
			loadedHarpoon = ((GameObject)Instantiate(harpoonPrefab)).transform;
		
		loadedHarpoon.parent = null;
		loadedHarpoon.position = new Vector3((float)h[Constants.STATUS_STARTPOS_X], (float)h[Constants.STATUS_STARTPOS_Y], (float)h[Constants.STATUS_STARTPOS_Z]);
		loadedHarpoon.rotation = Quaternion.Euler((float)h[Constants.STATUS_STARTROT_X],(float)h[Constants.STATUS_STARTROT_Y],(float)h[Constants.STATUS_STARTROT_Z]) ;
		
		float timeSinceFired = (float)(Game.instance.ServerTimeInMilliSeconds - (int)h[Constants.STATUS_STARTTIME]) / 1000f;
		
		loadedHarpoon.transform.position = loadedHarpoon.transform.TransformPoint(0f, 0f, _harpoonSpeed * timeSinceFired);
		loadedHarpoon.SendMessage("Fire");
		if (local) loadedHarpoon.SendMessage("SetLocal");
		else loadedHarpoon.SendMessage("SetRemote");
		loadedHarpoon = null; //destroy reference! it's out of our hands now.
	}
	
}
