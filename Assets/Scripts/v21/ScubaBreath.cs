using UnityEngine;
using System.Collections;

public class ScubaBreath : MonoBehaviour {
	
	
	private bool local = false;
	
	public float breathInterval;
	
	private float lastBreathTime;
	
	public float breathHoldingTime;
	public float ultimateBreathHoldingTime;
	public AnimationCurve weaknessCurve;
	public AnimationCurve blotchCurve;
	
	public ParticleSystem bubbles;
	
	public ScubaController controller;
	public GameObject camera;
	
	
	public AudioClip[] breathingSounds;
	public AudioClip[] gaspingSounds;
	
	void SetLocal() {
		local = true;	
	}
	
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (local) {
			if (!Input.GetKey(KeyCode.Space)) {
				if (Time.time > lastBreathTime + breathInterval) {
					lastBreathTime = Time.time;
					
					controller.weaknessModifier = 1;
					camera.GetComponent<NoiseEffect>().enabled = false;
					
					StartCoroutine("TriggerSoundEmit", breathingSounds[Random.Range(0, breathingSounds.Length - 1)]);
					
					Hashtable h = new Hashtable();
					
					h.Add(Constants.STATUS_COUNT, Random.Range(10, 20));
					
					Breathe(h);
					
					
					transform.SendMessage("LocalBreathe", h, SendMessageOptions.DontRequireReceiver);
					
				}
			}
			else {
				if (Time.time > lastBreathTime + breathHoldingTime) {
					Debug.Log("Running out of air");
					controller.weaknessModifier = weaknessCurve.Evaluate(Time.time - (lastBreathTime + breathHoldingTime));
					camera.GetComponent<NoiseEffect>().enabled = true;
					camera.GetComponent<NoiseEffect>().grainIntensityMax = blotchCurve.Evaluate(Time.time - (lastBreathTime + breathHoldingTime));
				}
				
				if (Time.time > lastBreathTime + ultimateBreathHoldingTime) {
					Debug.Log("Ran out of air");
					
					controller.weaknessModifier = 1;
					camera.GetComponent<NoiseEffect>().enabled = false;
					lastBreathTime = Time.time - 15;
					
					StartCoroutine("TriggerSoundEmit", gaspingSounds[Random.Range(0, gaspingSounds.Length - 1)]);
					
					
					Hashtable h = new Hashtable();
					
					h.Add(Constants.STATUS_COUNT, Random.Range(50, 60));
					
					Breathe(h);
					
					
					transform.SendMessage("LocalBreathe", h, SendMessageOptions.DontRequireReceiver);
					
				}
			}
		}
	}
	
	void Breathe (Hashtable h) {
		Debug.Log("Breathing", this);
		bubbles.Emit((int)h[Constants.STATUS_COUNT]);
	}
	
	IEnumerator TriggerSoundEmit(AudioClip clip) {
		yield return new WaitForSeconds(0.3f);
		audio.PlayOneShot(clip);
	}
}
