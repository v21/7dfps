using UnityEngine;
using System.Collections;

public class Harpoon : MonoBehaviour {
	private bool local = false;
	private bool fired = false;
	public Vector3 tipOffset;
	public void SetLocal() {
		local = true;	
		gameObject.layer = LayerMask.NameToLayer("HarpoonLocal");
	}
	
	public void SetRemote() {
		local = false;
		gameObject.layer = LayerMask.NameToLayer("HarpoonRemote");
	}
	
	public void Fire(){
		fired = true;
		collider.enabled = true;
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (fired) {
			transform.Translate(0f, 0f, HarpoonGun._harpoonSpeed * Time.deltaTime);
		}
	}
	
	void OnTriggerEnter (Collider collider) {
		Debug.Log("Harpoon : Collision!", this);
		
		Vector3 pos = transform.TransformPoint(tipOffset);
		
		collider.SendMessageUpwards("RecieveHarpoonHit", pos);
		
		
		if (collider.gameObject.layer == LayerMask.NameToLayer("Terrain") ||
			collider.gameObject.layer == LayerMask.NameToLayer("Surface"))
		{
			Debug.Log ("Cleanign up harpoon");
			Object.Destroy (this.gameObject);	
		}
		
	}
	
}
