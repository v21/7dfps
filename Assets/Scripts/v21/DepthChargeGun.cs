using UnityEngine;
using System.Collections;

public class DepthChargeGun : MonoBehaviour {
	
	private bool local = false;
	
	public GameObject depthChargePrefab;
	public GameObject uiExplosionPrefab;
	public GameObject uiExplosion;
	
	public Transform depthChargeStart;
	
	public float cooldownTime;
	private float lastFiredTime;
	
	public float depthChargeSpeed;
	public static float _depthChargeSpeed;
	
	private Transform loadedDepthCharge;
	
	private float buttonDownTime;
	
	public float previewSpeedFactor;
	
	public float startCharge;
	
	// Use this for initialization
	void Start () {
		_depthChargeSpeed = depthChargeSpeed;
		loadedDepthCharge = ((GameObject)Instantiate(depthChargePrefab, depthChargeStart.position, depthChargeStart.rotation)).transform;
		loadedDepthCharge.parent = transform;
		
		if (local) loadedDepthCharge.SendMessage("SetLocal");
		else loadedDepthCharge.SendMessage("SetRemote");
	}
	
	void SetLocal() {
		local = true;	
	}
	
	// Update is called once per frame
	void Update () {
		if (lastFiredTime + cooldownTime < Time.time) {
			if (loadedDepthCharge == null) {
				loadedDepthCharge = ((GameObject)Instantiate(depthChargePrefab, depthChargeStart.position, depthChargeStart.rotation)).transform;
				loadedDepthCharge.parent = transform;
				
				if (local) loadedDepthCharge.SendMessage("SetLocal");
				else loadedDepthCharge.SendMessage("SetRemote");
			}
		}
		if (local) {
			if (Input.GetButtonDown("Fire2") && !ChatScreen.chat) {
				if (loadedDepthCharge != null) {
					buttonDownTime = Time.time;
					if (uiExplosion == null){
						uiExplosion = (GameObject)Instantiate(uiExplosionPrefab, transform.position, transform.rotation);
					}
					else {
						uiExplosion.active = true;
					}
				}
			}
				
			if (Input.GetButtonUp("Fire2") && !ChatScreen.chat) {
				
				if (loadedDepthCharge != null) {
					
					
					float timeDown = Time.time - buttonDownTime;
					
					Vector3 endPos =  depthChargeStart.position + new Vector3(0f, _depthChargeSpeed * timeDown * previewSpeedFactor + startCharge, 0f);
					
					Hashtable h = new Hashtable();
					
					h.Add (Constants.STATUS_STARTPOS_X, depthChargeStart.position.x);
					h.Add (Constants.STATUS_STARTPOS_Y, depthChargeStart.position.y);
					h.Add (Constants.STATUS_STARTPOS_Z, depthChargeStart.position.z);
					
					h.Add (Constants.STATUS_STARTTIME, Game.instance.ServerTimeInMilliSeconds);
					
					h.Add (Constants.STATUS_ENDPOS_X, endPos.x);
					h.Add (Constants.STATUS_ENDPOS_Y, endPos.y);
					h.Add (Constants.STATUS_ENDPOS_Z, endPos.z);
					
					h.Add (Constants.STATUS_ENDTIME, Game.instance.ServerTimeInMilliSeconds + (int)((timeDown * previewSpeedFactor + (startCharge / _depthChargeSpeed)) * 1000f));
					
					FireDepthCharge(h);
					transform.SendMessage("LocalFireDepthCharge", h, SendMessageOptions.DontRequireReceiver);
					
					uiExplosion.active = false;
				}
			}
				
			if (Input.GetButton("Fire2") && !ChatScreen.chat) {
				
				if (loadedDepthCharge != null) {
					float timeDown = Time.time - buttonDownTime;
					Vector3 endPos =  depthChargeStart.position + new Vector3(0f, _depthChargeSpeed * timeDown * previewSpeedFactor + startCharge, 0f);
					
					uiExplosion.transform.position = endPos;
				}
			}
		}
	}
	
	void FireDepthCharge (Hashtable h) {
		lastFiredTime = Time.time;
		if (loadedDepthCharge == null)
			loadedDepthCharge = ((GameObject)Instantiate(depthChargePrefab)).transform;
		
		loadedDepthCharge.parent = null;
		loadedDepthCharge.position = new Vector3((float)h[Constants.STATUS_STARTPOS_X], (float)h[Constants.STATUS_STARTPOS_Y], (float)h[Constants.STATUS_STARTPOS_Z]);
		loadedDepthCharge.rotation = Quaternion.identity ;
		
		float timeSinceFired = (float)(Game.instance.ServerTimeInMilliSeconds - (int)h[Constants.STATUS_STARTTIME]) / 1000f;
		
		loadedDepthCharge.transform.position += new Vector3(0f, _depthChargeSpeed * timeSinceFired, 0f);
		
		
		loadedDepthCharge.SendMessage("Prime", h); //need to pass in the end coords & time
		
		if (local) loadedDepthCharge.SendMessage("SetLocal");
		else loadedDepthCharge.SendMessage("SetRemote");
		loadedDepthCharge = null; //destroy reference! it's out of our hands now.
	}
	
}
