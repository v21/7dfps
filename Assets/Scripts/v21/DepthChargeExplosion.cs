using UnityEngine;
using System.Collections;

public class DepthChargeExplosion : MonoBehaviour {
	
	private bool exploded;
	
	private float explodeTime;
	public float explosionDuration;
	public AnimationCurve sizeOverTime;
	private Vector3 startScale;
	public AnimationCurve lightRange;
	public AnimationCurve lightIntensity;
	public GameObject bubblesGO;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (exploded) {
			float scaleFactor = sizeOverTime.Evaluate(Time.time - explodeTime);
			transform.localScale = new Vector3(startScale.x * scaleFactor, startScale.y * scaleFactor, startScale.z * scaleFactor);
			light.range = lightRange.Evaluate(Time.time - explodeTime);
			light.intensity = lightIntensity.Evaluate(Time.time - explodeTime);
			if (explodeTime + explosionDuration < Time.time) {
				bubblesGO.transform.parent = null;
				Destroy(transform.parent.gameObject);
			}
		}
	}
	
	public void Explode() {
		
		Debug.Log("Exploded " + Time.time, this);
		exploded = true;
		explodeTime = Time.time;
		startScale = transform.localScale;
		bubblesGO.active = true;
	}
	
	public void OnTriggerEnter (Collider collider) {
		if (collider.gameObject.layer == LayerMask.NameToLayer("ScubaLocal") || collider.gameObject.layer == LayerMask.NameToLayer("ScubaRemote")){
			collider.SendMessageUpwards("RecieveDepthChargeHit");
		}
	}
	
}
