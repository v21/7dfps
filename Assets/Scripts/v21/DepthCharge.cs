using UnityEngine;
using System.Collections;

public class DepthCharge : MonoBehaviour {
	private bool local = false;
	private bool fired = false;
	private bool exploded = false;
	
	public float detonateTime = 0f;
	public Vector3 detonatePos;
	public GameObject explosion;
	
	
		
	public void SetLocal() {
		local = true;	
		gameObject.layer = LayerMask.NameToLayer("DepthChargeLocal");
		explosion.layer = LayerMask.NameToLayer("DepthChargeLocal");
	}
	
	public void SetRemote() {
		local = false;
		gameObject.layer = LayerMask.NameToLayer("DepthChargeRemote");
		explosion.layer = LayerMask.NameToLayer("DepthChargeRemote");
	}
	
	public void Prime(Hashtable h){
		detonatePos = new Vector3((float)h[Constants.STATUS_ENDPOS_X], (float)h[Constants.STATUS_ENDPOS_Y], (float)h[Constants.STATUS_ENDPOS_Z]);
		detonateTime = ((int)h[Constants.STATUS_ENDTIME] - Game.instance.ServerTimeInMilliSeconds) / 1000f + Time.time;
		
		Debug.Log (((int)h[Constants.STATUS_ENDTIME] - Game.instance.ServerTimeInMilliSeconds) / 1000f);
		fired = true;
	}
	
	// Use this for initialization
	void Start () {
		explosion.active = false;
		explosion.transform.GetChild(0).gameObject.active = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (fired && !exploded) {
			
			transform.Translate(0f, DepthChargeGun._depthChargeSpeed * Time.deltaTime, 0f , Space.World);
			
			if (Time.time > detonateTime) {
				Debug.Log("Explosion sent " + Time.time, this);
				//explode!
				explosion.active = true;
				renderer.enabled = false;
				explosion.SendMessage("Explode");
				exploded = true;
			}
		}
		
		

		
	}
	
	/*void OnCollisionEnter (Collision collision) {
		Debug.Log("DepthCharge : Collision!", this);
		
		collision.collider.SendMessageUpwards("RecieveDepthChargeHit", collision);
		
		
	}*/
	
}
