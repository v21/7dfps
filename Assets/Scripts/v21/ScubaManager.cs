using UnityEngine;
using System.Collections;

public class ScubaManager : MonoBehaviour {
	
	private bool local = false;
	
	public bool dead = false;
	
	public GameObject cam;
	public float respawnWait;
	
	public GameObject bloodCloudPrefab;
	private GameObject bloodCloud;
	
	public void SetLocal(){
		Debug.Log("SetLocal", this);
		local = true;
		cam.SetActiveRecursively(true);
		SetChildLayerRecursively(transform, LayerMask.NameToLayer("ScubaLocal"));
	}
	
	public void SetRemote(){
		Debug.Log("SetRemote", this);
		local = false;	
		cam.SetActiveRecursively(false);
		SetChildLayerRecursively(transform, LayerMask.NameToLayer("ScubaRemote"));
	}
	
	public void SetChildLayerRecursively(Transform trans, int layer) {
		trans.gameObject.layer = layer;
		foreach (Transform child in trans) { 
			SetChildLayerRecursively(child, layer);
		}
	}
	
	public void SetRenderersRecursively(Transform trans, bool enabled) {
		if (trans.renderer != null)
			trans.renderer.enabled = enabled;
		foreach (Transform child in trans) { 
			SetRenderersRecursively(child, enabled);
		}
	}
	
	
	public void RecieveDepthChargeHit() {
		Debug.Log("RecieveDepthChargeHit");
		if (local) {
			Hashtable h = new Hashtable();
			DepthChargeHit(h);
			SendMessage("LocalDepthChargeHit", h);
		}
	}
	
	public void DepthChargeHit(Hashtable h) {
		dead = true;
		gameObject.GetComponent<ScubaController>().enabled = false;
		gameObject.GetComponent<HarpoonGun>().enabled = false;
		gameObject.GetComponent<DepthChargeGun>().enabled = false;
		
		SetRenderersRecursively(transform, false);
		
		bloodCloud = (GameObject)Instantiate(bloodCloudPrefab, transform.position, transform.rotation);
		bloodCloud.transform.parent = transform;
		
		StartCoroutine("Respawn");
			
		if (local){
			usePhoton.instance.SetDisplayMessage("Died from a depth charge", "You are dead", respawnWait);
			rigidbody.constraints = RigidbodyConstraints.None;
		}
	}
	
	public void RecieveHarpoonHit(Vector3 point) {
		Debug.Log("ReceieveHarpoonHit");
		if (local) {
			Debug.Log("And was local", this);
			Hashtable h = new Hashtable();
			h.Add(Constants.STATUS_STARTPOS_X, point.x);
			h.Add(Constants.STATUS_STARTPOS_Y, point.y);
			h.Add(Constants.STATUS_STARTPOS_Z, point.z);
			HarpoonHit(h);
			SendMessage("LocalHarpoonHit", h);
			
		}
	}
	
	public void HarpoonHit(Hashtable h) {
		//actually do shit
		
		Vector3 hitPos = new Vector3((float)h[Constants.STATUS_STARTPOS_X], (float)h[Constants.STATUS_STARTPOS_Y], (float)h[Constants.STATUS_STARTPOS_Z]);
		
		
		dead = true;
		gameObject.GetComponent<ScubaController>().enabled = false;
		gameObject.GetComponent<HarpoonGun>().enabled = false;
		gameObject.GetComponent<DepthChargeGun>().enabled = false;
		
		bloodCloud = (GameObject)Instantiate(bloodCloudPrefab, hitPos, transform.rotation);
		bloodCloud.transform.parent = transform;
		
		StartCoroutine("Respawn");
			
		if (local){
			usePhoton.instance.SetDisplayMessage("Died from a harpoon", "You are dead", respawnWait);
			rigidbody.constraints = RigidbodyConstraints.None;
		}
		
	}
	
	public IEnumerator Respawn() {
		yield return new WaitForSeconds(respawnWait);
		
		SetRenderersRecursively(transform, true);
		
		if (bloodCloud != null)
			bloodCloud.transform.parent = null; //ugh : nasty race condition here? will deal when we corpsify shit
		
		if (local) {
			usePhoton.instance.PlaceLocalPlayer(transform);
		}
		
		dead = false;
		gameObject.GetComponent<ScubaController>().enabled = true;
		gameObject.GetComponent<HarpoonGun>().enabled = true;
		gameObject.GetComponent<DepthChargeGun>().enabled = true;
		rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
	}
	
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
