
using UnityEngine;
using System.Collections;

public class Flying : MonoBehaviour {
	
	public GameObject animatedObject;
	public float animationAdjustmentFactor;
	
	public bool useGroundEffect;
	public bool useFakeGravity;
	
	public float rollLevelling;
	
	public float pitchMultiplier;
	public float yawMultiplier;
	public float rollMultiplier;
	
	public float roll = 0f;
	public float pitch = 0f;
	public float yaw = 0f;
	
	public float speed = 0f;
	
	public float minAxis;
	public float maxAxis;
	
	
	public float inputSmoothFactor;
	
	public float speedLerpFactor;
	
	public float forwardSpeed;
	public float boostFactor;
	
	
	
	public float groundEffectRaycastDistance;
	public AnimationCurve groundEffectCurve;
	public float groundEffect; //for display purposes
	
	
	public Vector3 targetExternalForces;
	public Vector3 currentExternalForces;
	public float externalForcesLerpFactor;
	
	public AnimationCurve fakePitchGravityCurve;
	public float fakePitchGravity;
	
	
	int? activeDustcloudI = null;
	
	public LayerMask raycastMask; //everything but Fruit
	
	void Start ()
	{
		Screen.lockCursor = true;
	}
	void FixedUpdate () {
		
		
		//Quaternion newLocalRotation = Quaternion.Euler(Input.GetAxis ("Vertical") * Time.deltaTime, Input.GetAxis ("Horizontal") * Time.deltaTime, 0);
		
		/*Debug.Log(transform.localRotation.w + " " +
		          transform.localRotation.x + " " +
		          transform.localRotation.y + " " +
		          transform.localRotation.z + " " +
		          " * " +  
		          newLocalRotation.w + " " +
		          newLocalRotation.x + " " +
		          newLocalRotation.y + " " +
		          newLocalRotation.z + " " +
		           " = " + 
		          (transform.localRotation * newLocalRotation).w + " " +
		          (transform.localRotation * newLocalRotation).x + " " +
		          (transform.localRotation * newLocalRotation).y + " " +
		          (transform.localRotation * newLocalRotation).z + " "
		          );
		 */
		
		//Debug.Log(transform.localRotation * newLocalRotation);
		
		//transform.localRotation = transform.localRotation * newLocalRotation;
		//I get essentially the same thing when I run this:
		//transform.Rotate(Input.GetAxis ("Vertical"), Input.GetAxis ("Horizontal"), 0);
		
		
		//Debug.Log(new Quaternion(0.9539526f, 0.04259391f, 0.06234282f, 0.2902994f) * new Quaternion(1f, 0f, 0f, 0f));
		//0.9539526 0.04259391 0.06234282 0.2902994  * 1 0 0 0  = NaN 0.04259391 0.06234282 0.2902994 
		
		
		pitch = Mathf.Lerp((Mathf.Clamp(Input.GetAxis ("Vertical"), minAxis, maxAxis) * pitchMultiplier), pitch, inputSmoothFactor );
		yaw = Mathf.Lerp((Mathf.Clamp(Input.GetAxis ("Horizontal"), minAxis, maxAxis) * yawMultiplier), yaw, inputSmoothFactor );
		roll = Mathf.Lerp((Mathf.Clamp(Input.GetAxis ("Horizontal") , minAxis, maxAxis) * rollMultiplier), roll, inputSmoothFactor);
		
		
		/*
		float pitch = Input.GetAxis ("Vertical");
		float yaw = Input.GetAxis ("Horizontal");
		float roll = 0;
		*/
		
		
		//get current pitch and roll
		float currPitch = transform.rotation.eulerAngles.x;
		if (currPitch > 180) {
			currPitch = currPitch - 360; //convert to -180 to 180
		}
		
		//float currYaw = transform.rotation.eulerAngles.y;
		
		
		float currRoll = transform.rotation.eulerAngles.z;
		if (currRoll > 180) {
			currRoll = currRoll - 360; //convert to -180 to 180
		}
		
		//don't level off a roll if we're going straight up or down
		if (60 > Mathf.Abs (currPitch)) {
			roll -= rollLevelling * currRoll;
		}
		
		
		
		//Quaternion newLocalRotation = Quaternion.Euler(pitch, yaw, roll);
		
		//transform.localRotation *= newLocalRotation;
		
		//rigidbody.MoveRotation(rigidbody.rotation * newLocalRotation);
		
		//transform.localRotation = rigidbody.rotation * newLocalRotation;
		
		
		rigidbody.rotation *= Quaternion.Euler(pitch, yaw, roll);
		//transform.Rotate(pitch, yaw, roll);
		
		
		
		
		//apply acceleration if near the ground
		
		if (useGroundEffect){
		RaycastHit hit = new RaycastHit();
		Ray ray = new Ray(transform.position, - transform.up);
		
		if (Physics.Raycast(transform.position, -transform.up, out hit, groundEffectRaycastDistance, raycastMask)){
			groundEffect = groundEffectCurve.Evaluate( hit.distance);
			
			//get type of terrain underneath, for fancy dustcloud purposes
			
			//SetDustcloud(hit);
				
		}
		else {
			groundEffect = 1;
		}
		
		}
		else {
			groundEffect = 1;
		}
		
		currentExternalForces = Vector3.Lerp(currentExternalForces, targetExternalForces, externalForcesLerpFactor);
		
		
		if (useFakeGravity){
			//go faster when going down, go slower when going up.
			fakePitchGravity = fakePitchGravityCurve.Evaluate (currPitch);
			
		}
		else {
			fakePitchGravity = 1;
		
		}
		
		speed = Mathf.Lerp(forwardSpeed * groundEffect * fakePitchGravity, speed, speedLerpFactor * Time.deltaTime);
		
		Vector3 forwardVector = Vector3.forward * speed  + currentExternalForces;
		
		
		if (Input.GetButton("Boost")){
			speed = Mathf.Lerp(forwardSpeed * groundEffect * fakePitchGravity * boostFactor, speed, speedLerpFactor * Time.deltaTime);
		
			
		}
		else{
			speed = Mathf.Lerp(forwardSpeed * groundEffect * fakePitchGravity, speed, speedLerpFactor * Time.deltaTime);
		}
		
		if (animatedObject != null){
			foreach (AnimationState state in animatedObject.animation) {
	            state.speed = speed / animationAdjustmentFactor;
	        }
		}
		
		rigidbody.velocity = rigidbody.rotation * forwardVector;
		//transform.Translate(forwardVector);
		
	}
	
	/*
	void OnTriggerEnter () {
		Debug.LogError("Collision!");	
		//speed = speed * collisionFactor;
		forwardSpeed = 0;
		
	}
	void OnTriggerExit () {
		//Debug.LogError("Collision!");	
		//speed = speed * collisionFactor;
		forwardSpeed = 0.2f;
		
	}*/
	
	
	void OnTriggerEnter (Collider other) {
		
		
		
		Debug.Log("trigger enter " + other.tag);
		
	}
	
	
	void OnTriggerExit (Collider other) {
		
	}
	
	
	void OnCollisionEnter (Collision collision) {
		//Debug.LogError("Collision!");	
		speed = 0f; // speed * collisionFactor;
		
	}
	
	
	
	
	
}
