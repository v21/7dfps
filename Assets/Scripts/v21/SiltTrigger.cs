using UnityEngine;
using System.Collections;

public class SiltTrigger : MonoBehaviour {
	
	public GameObject siltCloudPrefab;
	public Vector3 siltOffsetFromScuba;
	public float absY;
	public float cooldownPeriod;
	private float lastTrigger;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerStay(Collider collider) {
		if (lastTrigger + cooldownPeriod < Time.time)
		{
			lastTrigger = Time.time;
			if (collider.gameObject.layer == LayerMask.NameToLayer("ScubaLocal")){
				
				Hashtable h = new Hashtable();
				Vector3 startPos = collider.transform.TransformPoint(siltOffsetFromScuba);
				h.Add (Constants.STATUS_STARTPOS_X, startPos.x);
				h.Add (Constants.STATUS_STARTPOS_Y, absY);
				h.Add (Constants.STATUS_STARTPOS_Z, startPos.z);
				
				SpawnSiltcloud(h);
				Game.instance.OpRaiseEvent(Constants.EV_SILTCLOUD, h, true, 0);
			}
		}
	}
	
	void SpawnSiltcloud(Hashtable h) {
		Instantiate(siltCloudPrefab, 
				new Vector3((float)h[Constants.STATUS_STARTPOS_X], (float)h[Constants.STATUS_STARTPOS_Y], (float)h[Constants.STATUS_STARTPOS_Z]),
				Quaternion.identity);
	}
}
